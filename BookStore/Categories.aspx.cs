﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BookStore.App_Data;
using Oracle.ManagedDataAccess.Client;

namespace BookStore
{
	public partial class Categories : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if(IsPostBack)
				return;

			UpdateList(-1);
		}

		private void UpdateList(int selectedId)
		{
			List<Category> items;

			using (var c = new DatabaseContainer())
				items = c.CATEGORIES_GET().ToList();

			lbCategories.DataValueField = "Id";
			lbCategories.DataTextField = "Name";

			lbCategories.DataSource = items;
			lbCategories.DataBind();

			var itemToSelect = lbCategories.Items.OfType<ListItem>().SingleOrDefault(item => int.Parse(item.Value) == selectedId);

			if (itemToSelect == null)
			{
				if (lbCategories.Items.Count > 0)
					lbCategories.SelectedIndex = 0;
			}
			else
			{
				itemToSelect.Selected = true;
			}

			UpdateText();
		}

		protected void lbCategories_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			UpdateText();
		}

		protected void bUpdate_OnClick(object sender, EventArgs e)
		{
			lStatus.Text = "";

			if(!Page.IsValid)
				return;

			if(lbCategories.SelectedItem == null)
				return;

			var id = int.Parse(lbCategories.SelectedItem.Value);
			var name = lbCategories.SelectedItem.Text;

			if(name.Equals(tbName.Text))
				return;

			//todo ad: check for category unqiue and invalid id

			var status = ExceptionHelper.ReturnBusinessErrorOrRedirect(() =>
			{
				using (var c = new DatabaseContainer())
					c.CATEGORIES_UPDATE(id, tbName.Text);

			},
				"Failed to update category",
				Response);

			switch (status.Status)
			{
				case ExceptionHelper.Status.InternalError:
					return;
				case ExceptionHelper.Status.BusinessError:
					lStatus.Text = status.Error;
					return;
			}

			UpdateList(id);
		}

		protected void bAdd_OnClick(object sender, EventArgs e)
		{
			lStatus.Text = "";

			if(!Page.IsValid)
				return;

			var pId = new ObjectParameter("p_id", typeof(int));

			var status = ExceptionHelper.ReturnBusinessErrorOrRedirect(() =>
			{
				using (var c = new DatabaseContainer())
					c.CATEGORIES_ADD(tbName.Text, pId);
			},
			"Failed to add category", 
			Response);

			if (status.Status == ExceptionHelper.Status.BusinessError)
				lStatus.Text = status.Error;

			switch (status.Status)
			{
				case ExceptionHelper.Status.BusinessError:
					lStatus.Text = status.Error;
					return;
				case ExceptionHelper.Status.Success:
					UpdateList((int)(decimal)pId.Value);
					return;
			}
		}

		protected void bDelete_OnClick(object sender, EventArgs e)
		{
			lStatus.Text = "";

			if (lbCategories.SelectedItem == null)
				return;

			var id = int.Parse(lbCategories.SelectedItem.Value);

			var status = ExceptionHelper.ReturnBusinessErrorOrRedirect(() =>
			{
				using (var c = new DatabaseContainer())
					c.CATEGORIES_DELETE(id);
			},
				"Failed to remove category",
				Response);

			switch (status.Status)
			{
				case ExceptionHelper.Status.BusinessError:
					lStatus.Text = status.Error;
					return;
				case ExceptionHelper.Status.Success:
					UpdateList(-1);
					return;
			}
		}

		private void UpdateText()
		{
			if (lbCategories.SelectedItem == null)
				return;

			tbName.Text = lbCategories.SelectedItem.Text;
		}
	}
}