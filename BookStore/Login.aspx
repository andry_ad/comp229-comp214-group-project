﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="BookStore.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <div class="title">
        <span class="title_icon">
            <img src="images/bullet1.gif" alt="" title="" /></span>My account
    </div>

    <div class="feat_prod_box_details">
        <p class="details">
        </p>

        <div class="contact_form">
            <div class="form_subtitle">login into your account</div>
            <div class="form_row">
                <label class="contact"><strong>Email:</strong></label>
                <asp:TextBox runat="server" CssClass="contact_input" ID="tbEmail" />
            </div>

            <div class="form_row">
                <label class="contact"><strong>Password:</strong></label>
                <asp:TextBox runat="server" TextMode="Password" CssClass="contact_input" ID="tbPassword" />
            </div>

            <div class="form_row">
                <div class="terms">
                    <asp:CheckBox runat="server" ID="cbRemember" Text="Remember me" />
                </div>
            </div>

            <div class="form_row">
                <asp:Label runat="server" ID="lError" ForeColor="Red" />
            </div>

            <div class="form_row">
                <asp:Button runat="server" ID="bLogin" CssClass="register" Text="Login" OnClick="bLogin_OnClick" />
            </div>
        </div>

    </div>

    <div class="clear"></div>
</asp:Content>
