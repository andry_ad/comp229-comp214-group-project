﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Promoes.aspx.cs" Inherits="BookStore.Promoes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    
    <asp:Button runat="server" Text="Add" CssClass="register" ID="bAdd" OnClick="bAdd_OnClick"/>

        <asp:Repeater ID="rPromoes" runat="server" OnItemCommand="rPromoes_OnItemCommand">

        <HeaderTemplate>
            <table border="0" width="100%">
                <tr>
                    <th>Id</th>
                    <th>Code</th>
                    <th>Expiry</th>
                    <th>Administrator</th>
                    <th>Discount</th>
                    <th>Edit</th>
                </tr>
        </HeaderTemplate>

        <ItemTemplate>
            <tr>
                <td><%# Eval("Id") %></td>
                <td><%# Eval("Code") %></td>
                <td><%# Eval("Expiry") %></td>
                <td><%# Eval("Email") %></td>
                <td><%# Eval("Discount") %>%</td>
                <td><asp:Button runat="server" Text="Edit" CommandName="Edit" CommandArgument='<%# Eval("Id") %>' CssClass="register" UseSubmitBehavior="False"/></td>
            </tr>
        </ItemTemplate>

        <FooterTemplate>
            </table>
        </FooterTemplate>

    </asp:Repeater>

</asp:Content>