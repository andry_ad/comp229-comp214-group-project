﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BookStore
{
	public partial class Error : System.Web.UI.Page
	{
		private const string ParamMessage = "message";

		protected string ErrorMessage { get; private set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			if(IsPostBack)
				return;

			ErrorMessage = Request.QueryString[ParamMessage] ?? "Internal error";
		}

		public static string GetErrorPageUrl(string errorMessage)
		{
			return string.Format("Error.aspx?{0}={1}", ParamMessage, errorMessage);
		}
	}
}