﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="BookStore.Register" %>
<%@ Import Namespace="BookStore" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <div class="title">
        <span class="title_icon">
            <img src="images/bullet1.gif" alt="" title="" /></span>Register
    </div>

    <div class="feat_prod_box_details">
        <div class="contact_form">
            <div class="form_subtitle">create new account</div>
            <div class="form_row">
                <label class="contact"><strong>Email:</strong></label>
                <asp:TextBox runat="server" CssClass="contact_input" ID="tbEmail" />
                
                <asp:RequiredFieldValidator runat="server" ControlToValidate="tbEmail" ErrorMessage="'Email' field must be set" Display="None" />
                <asp:CustomValidator runat="server" ID="vUniqueEmail" ControlToValidate="tbEmail" Display="None" OnServerValidate="vUniqueEmail_OnServerValidate" ErrorMessage="User with same email address already exists" />
            </div>

            <div class="form_row">
                <label class="contact"><strong>Password:</strong></label>
                <asp:TextBox runat="server" TextMode="Password" CssClass="contact_input" ID="tbPassword" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="tbPassword" ErrorMessage="'Password' field must be set" Display="None" />
            </div>

            <div class="form_row">
                <label class="contact"><strong>First Name:</strong></label>
                <asp:TextBox runat="server" CssClass="contact_input" ID="tbFirstName" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="tbFirstName" ErrorMessage="'First Name' field must be set" Display="None" />
            </div>

            <div class="form_row">
                <label class="contact"><strong>Last Name:</strong></label>
                <asp:TextBox runat="server" CssClass="contact_input" ID="tbLastName" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="tbLastName" ErrorMessage="'Last Name' field must be set" Display="None" />
            </div>

            <div class="form_row">
                <label class="contact"><strong>Date of birth:</strong></label>
                <asp:TextBox runat="server" CssClass="contact_input" ID="tbBirthday" />
                <asp:RequiredFieldValidator runat="server" ID="vBirthday" ControlToValidate="tbBirthday" ErrorMessage="'Date of birth' field must be set" Display="None" />
                <asp:RangeValidator ID="vRBirthday" runat="server" ControlToValidate="tbBirthday" ErrorMessage="Invalid 'Date of birth'" Type="Date" Display="None" />
            </div>

            <div class="form_row">
                <label class="contact"><strong>Phone:</strong></label>
                <asp:TextBox runat="server" CssClass="contact_input" ID="tbPhone" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="tbPhone" ErrorMessage="'Phone' field must be set" Display="None" />
            </div>

            <div class="form_row">
                <label class="contact"><strong>Address:</strong></label>
                <asp:TextBox runat="server" CssClass="contact_input" ID="tbAddress" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="tbAddress" ErrorMessage="'Address' field must be set" Display="None" />
            </div>

            <div class="form_row">
                <label class="contact"><strong>Postal Code:</strong></label>
                <asp:TextBox runat="server" CssClass="contact_input" ID="tbPostalCode" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="tbPostalCode" ErrorMessage="'Postal Code' field must be set" Display="None" />
            </div>

            <div class="form_row">
                <label class="contact"><strong>City:</strong></label>
                <asp:TextBox runat="server" CssClass="contact_input" ID="tbCity" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="tbCity" ErrorMessage="'City' field must be set" Display="None" />
            </div>

            <div class="form_row">
                <label class="contact"><strong>State:</strong></label>
                <asp:DropDownList runat="server" ID="ddState" CssClass="contact_input" />
            </div>

            <div class="form_row">
                <asp:ValidationSummary runat="server" HeaderText="Please fix next error(s):" ShowMessageBox="False" DisplayMode="BulletList" ShowSummary="True" ForeColor="Red" />
            </div>

            <div class="form_row">
                <asp:Button runat="server" CssClass="register" Text="Register" OnClick="OnClick" />
            </div>
        </div>

    </div>
    <div class="clear"></div>
</asp:Content>
