﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using BookStore.App_Data;

namespace BookStore
{
	public partial class Login : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
		}

		protected void bLogin_OnClick(object sender, EventArgs e)
		{
			if(!Page.IsValid)
				return;

			var r = MySession.Login(tbEmail.Text, tbPassword.Text, cbRemember.Checked);

			if (r)
			{
				var a = MySession.IsUserLoged;

				Response.Redirect("Index.aspx");
			}
			else
				lError.Text = "Invalid email or password!";
		}
	}
}