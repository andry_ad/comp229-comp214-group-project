﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web.UI.WebControls;
using BookStore.App_Data;

namespace BookStore
{
	public partial class Checkout : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack)
				return;

			if (!MySession.IsUserLoged)
			{
				Response.Redirect("Index.aspx");
				return;
			}

			UpdateBooks();
		}

		private void UpdateBooks()
		{
			List<BASKET_GET_BY_USER_Result> books;
			try
			{
				CartDetails cart = null;
				using (var c = new DatabaseContainer())
				{
					books = c.BASKET_GET_BY_USER(MySession.UserOrAdminId).ToList();
					cart = Cart.BasketGetTotal(c, MySession.UserOrAdminId);
				}

				lTotal.Text = cart.TotalPrice.ToString();
				lTotalTax.Text = cart.TotalTax.ToString();
				lTotalAndTax.Text = cart.TotalGrand.ToString();
			}
			catch
			{
				Response.Redirect(BookStore.Error.GetErrorPageUrl("Failed to get books. Please try again later"));
				return;
			}

			rBooks.DataSource = books;
			rBooks.DataBind();
		}

		protected void bConfirm_OnClick(object sender, EventArgs e)
		{
			if(!Page.IsValid)
				return;

			var promo = string.IsNullOrWhiteSpace(tbPromo.Text)
				            ? null
				            : tbPromo.Text;


			var basketId = new ObjectParameter("p_basketId", typeof(int));

			using (var c = new DatabaseContainer())
				c.BASKET_CHECKOUT(MySession.UserOrAdminId, promo, basketId);

			var cm = new CheckoutMessage();
			cm.Id = (int)(decimal)basketId.Value;

			var id = (int)(decimal)basketId.Value;

			Helper.ShowPopUpMsg(Page, "Thank you for your purchase!", OrderHistory.GetOrderIdPageUrl(id));
		}

		protected void bCancel_OnClick(object sender, EventArgs e)
		{
			Response.Redirect("Index.aspx");
		}

		protected void bToCart_OnClick(object sender, EventArgs e)
		{
			Response.Redirect("Cart.aspx");
		}

		protected void vCode_OnServerValidate(object source, ServerValidateEventArgs args)
		{
			if (string.IsNullOrWhiteSpace(tbPromo.Text))
				return;

			var p_discount = new ObjectParameter("p_discount", typeof (int));

			var s = ExceptionHelper.ReturnBusinessErrorOrRedirect(() =>
			{
				using (var c = new DatabaseContainer())
					c.PROMOES_GET_DISCOUNT(tbPromo.Text, p_discount);
			},
				"Failed to check promo code",
				Response);

			switch (s.Status)
			{
				case ExceptionHelper.Status.BusinessError:
					args.IsValid = false;
					break;
			}
		}
	}
}