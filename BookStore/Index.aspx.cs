﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BookStore.App_Data;

namespace BookStore
{
	public partial class Index : System.Web.UI.Page
	{
		private const string CategoryId = "Category";

		protected List<BOOKS_GETSHORTDESCRIPTION_Result> Books { get; private set; }

		protected string CategoryName { get; private set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			var categoryId = -1;

			var sId = Request.QueryString[CategoryId];

			if (!string.IsNullOrWhiteSpace(sId))
			{
				try
				{
					categoryId = int.Parse(sId);
				}
				catch
				{
					Response.Redirect(BookStore.Error.GetErrorPageUrl("Invalid category id"));
					return;
				}
			}

			try
			{
				using (var c = new DatabaseContainer())
				{
					if (categoryId == -1)
						CategoryName = "All";
					else
					{
						var pName = new ObjectParameter("p_name", typeof(string));
						c.CATEGORIES_GET_NAME_BY_ID(categoryId, pName);

						CategoryName = (string)pName.Value;
					}

					Books = c.BOOKS_GETSHORTDESCRIPTION(categoryId).ToList();
				}
			}
			catch 
			{
				Response.Redirect(BookStore.Error.GetErrorPageUrl("Invalid category"));
				return;
			}
		}

		public static string GetUrlForCategoryId(int id)
		{
			return string.Format("Index.aspx?{0}={1}", CategoryId, id);
		}

		public static string AdjustTitleLength(string title)
		{
			const int maxTitleLength = 20;

			if (title.Length <= maxTitleLength)
				return title;

			return title.Substring(0, maxTitleLength - 3) + "...";
		}
	}
}