﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using BookStore.App_Data;

namespace BookStore
{
	public partial class Inventory : Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if(IsPostBack)
				return;

			List<BOOKS_GET_Result> books;

			try
			{
				using (var c = new DatabaseContainer())
					books = c.BOOKS_GET().ToList();
			}
			catch 
			{
				Response.Redirect(BookStore.Error.GetErrorPageUrl("Failed to get books. Please try again later"));
				return;
			}

			rBooks.DataSource = books;
			rBooks.DataBind();
		}

		protected void bAdd_OnClick(object sender, EventArgs e)
		{
			Response.Redirect("EditBook.aspx");
		}

		protected void rBooks_OnItemCommand(object source, RepeaterCommandEventArgs e)
		{
			if(e.CommandName != "Edit")
				return;

			Response.Redirect(EditBook.GetUrlForBookId(int.Parse((string)e.CommandArgument)));
		}
	}
}