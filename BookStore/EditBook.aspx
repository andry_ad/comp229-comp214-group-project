﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="EditBook.aspx.cs" Inherits="BookStore.EditBook" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">

    <table>
        <tr>
            <td>
                <label class="contact"><strong>Title:</strong></label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbTitle" CssClass="contact_input" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="tbTitle" Display="None" ErrorMessage="'Title' field must be set" />
            </td>
        </tr>

        <tr>
            <td>
                <label class="contact"><strong>ISBN:</strong></label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbISBN" CssClass="contact_input" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="tbISBN" Display="None" ErrorMessage="'ISBN' field must be set"/>
                <asp:CustomValidator runat="server" ValidateEmptyText="True" ID="vUniqueISBN" ControlToValidate="tbISBN" OnServerValidate="vUniqueISBN_OnServerValidate" ErrorMessage="ISBN value must be unique" Display="None"/>
                <asp:RegularExpressionValidator runat="server" ControlToValidate="tbISBN" ErrorMessage="'ISBN' invalid format (digits, '-', spaces, letters)" Display="None" ValidationExpression="[0-9A-za-z- ]{10,}"/>
            </td>
        </tr>

        <tr>
            <td>
                <label class="contact"><strong>Author:</strong></label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbAuthor" CssClass="contact_input" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="tbAuthor" Display="None" ErrorMessage="'Author' field must be set"/>
            </td>
        </tr>

        <tr>
            <td>
                <label class="contact"><strong>Publisher:</strong></label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbPublisher" CssClass="contact_input" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="tbPublisher" Display="None" ErrorMessage="'Publisher' field must be set"/>
            </td>
        </tr>

        <tr>
            <td>
                <label class="contact"><strong>Year:</strong></label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbYear" CssClass="contact_input" />
                <asp:RequiredFieldValidator ID="vReqYear" runat="server" ControlToValidate="tbYear" Display="None" ErrorMessage="'Year' field must be set"/>
                <asp:RangeValidator ID="vRngYear" runat="server" ControlToValidate="tbYear" Display="None" ErrorMessage="'Year' field is invalid" Type="Integer" MinimumValue="1900"/>
            </td>
        </tr>

        <tr>
            <td>
                <label class="contact"><strong>Price:</strong></label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbPrice" CssClass="contact_input" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="tbPrice" Display="None" ErrorMessage="'Price' field must be set"/>
            </td>
        </tr>

        <tr>
            <td>
                <label class="contact"><strong>Search index:</strong></label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbSearchCounts" CssClass="contact_input" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="tbSearchCounts" Display="None" ErrorMessage="'Search index:' field must be set"/>
            </td>
        </tr>

        <tr>
            <td>
                <label class="contact"><strong>Picture:</strong></label>
            </td>
            <td>
                <asp:Image runat="server" ID="iBookPicture" CssClass="normalThumb"/>

                <br />
                <br />
                <asp:FileUpload runat="server" ID="fuImage"/>
                <br />
                <br />
                <asp:Button runat="server" ID="bUpload" Text="Upload" OnClick="bUpload_OnClick" CausesValidation="False" />
                <asp:Label runat="server" Font-Italic="True" Text="Select file and click 'Upload'"/>
                <br />
                <br />
                <asp:Label runat="server" ID="lFileSatuts" />
                <asp:CustomValidator runat="server" ValidateEmptyText="True" ID="vImageUploaded" ControlToValidate="fuImage" OnServerValidate="vImageUploaded_OnServerValidate" ErrorMessage="Image must be set" Display="None"/>
            </td>
        </tr>

        <tr>
            <td>
                <label class="contact"><strong>Categories:</strong></label>
            </td>
            <td>
                <asp:CheckBoxList runat="server" ID="cblCategories" />
            </td>
        </tr>
        
        <tr>
            <td>
            </td>
            <td>
                <asp:ValidationSummary runat="server" HeaderText="Please fix next error(s):" ShowMessageBox="False" DisplayMode="BulletList" ShowSummary="True" ForeColor="Red" />
            </td>
        </tr>

    </table>

    <asp:Button runat="server" CssClass="register" ID="bSave" OnClick="bSave_OnClick"/>

</asp:Content>
