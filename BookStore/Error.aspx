﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="BookStore.Error" %>
<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    
    <h1 style="color: red"><%= ErrorMessage %></h1>

</asp:Content>
