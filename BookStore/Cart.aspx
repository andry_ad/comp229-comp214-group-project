﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Cart.aspx.cs" Inherits="BookStore.Cart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
	<div class="title">
        <span class="title_icon">
            <img src="images/bullet1.gif" alt="" title="" /></span>My cart
    </div>

    <div class="feat_prod_box_details">

        <table class="cart_table">
            <tr class="cart_title">
                <td>Item pic</td>
                <td>Book name</td>
                <td>Unit price</td>
                <td>Qty</td>
                <td>Total</td>
                <td>Change Qty</td>
                <td>Delete</td>
            </tr>
            <asp:Repeater ID="rBooks" runat="server" OnItemCommand="rBooks_OnItemCommand">
                <ItemTemplate>

                    <tr>
                        <td>
                            <img src="productImages/<%# Eval("ISBN") %>.jpg" alt="<%# Eval("ISBN") %>" class="cart_thumb" /></td>
                        <td style="text-align: left"><%# Eval("Title") %></td>
                        <td><%# Eval("PRICE") %>$</td>
                        <td><%# Eval("Quantity") %></td>
                        <td><%# Eval("SUBTOTAL") %>$</td>
						<td>
                            <asp:TextBox runat="server" ID="tbQty" Text='<%# Eval("Quantity") %>' Width="40" />
                            <asp:LinkButton runat="server" Text="Update" ID="bUpdate" CommandName="Update" CommandArgument='<%# Eval("BookId") %>' UseSubmitBehavior="False"/>
                        </td>
                        <td><asp:LinkButton runat="server" Text="X" ID="bRemove" CommandName="Remove" CommandArgument='<%# Eval("BookId") %>' UseSubmitBehavior="False"/></td>
                    </tr>

                </ItemTemplate>
            </asp:Repeater>

            <tr>
                <td colspan="6" class="cart_total"><span class="red">TOTAL:</span></td>
                <td>
                    <asp:Label runat="server" ID="lTotal" />$</td>
            </tr>

            <tr>
                <td colspan="6" class="cart_total"><span class="red">TOTAL TAX:</span></td>
                <td>
                    <asp:Label runat="server" ID="lTotalTax" />$</td>
            </tr>

            <tr>
                <td colspan="6" class="cart_total"><span class="red">TOTAL + TAX:</span></td>
                <td>
                    <asp:Label runat="server" ID="lTotalAndTax" />$</td>
            </tr>
        </table>

        <asp:Button runat="server" CssClass="register" Text="Checkout" ID="bCheckout"  OnClick="bCheckout_OnClick"/>
        <asp:Button runat="server" CssClass="register" Text="Continue Shopping" ID="bCancel"  OnClick="bCancel_OnClick"/>

    </div>

    <div class="clear"></div>
</asp:Content>
