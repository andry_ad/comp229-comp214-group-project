﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="OrderHistory.aspx.cs" Inherits="BookStore.OrderHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">

	<div class="title">
		<span class="title_icon">
			<img src="images/bullet1.gif" alt="" title="" /></span>Order #<asp:Label runat="server" ID="lOrderId" Font-Bold="True"/>
	</div>

	<div class="feat_prod_box_details">

		<table class="cart_table">
			<tr class="cart_title">
				<td>Item pic</td>
				<td>Book name</td>
				<td>Unit price</td>
				<td>Qty</td>
				<td>Total</td>
			</tr>
			<asp:Repeater ID="rBooks" runat="server">
				<ItemTemplate>
					<tr>
						<td>
							<img src="productImages/<%# Eval("ISBN") %>.jpg" alt="<%# Eval("ISBN") %>" class="cart_thumb" /></td>
						<td style="text-align: left"><%# Eval("Title") %></td>
						<td><%# Eval("PRICE") %>$</td>
						<td><%# Eval("Quantity") %></td>
						<td><%# Eval("SUBTOTAL") %>$</td>
					</tr>

				</ItemTemplate>
			</asp:Repeater>

			<tr>
				<td colspan="4" class="cart_total"><span class="red">TOTAL:</span></td>
				<td>
					<asp:Label runat="server" ID="lTotal" />$</td>
			</tr>
			
						<tr>
				<td colspan="4" class="cart_total"><span class="red">DISCOUNT:</span></td>
				<td>
					<asp:Label runat="server" ID="lDiscount" />%</td>
			</tr>

			<tr>
				<td colspan="4" class="cart_total"><span class="red">TOTAL TAX:</span></td>
				<td>
					<asp:Label runat="server" ID="lTotalTax" />$</td>
			</tr>

			<tr>
				<td colspan="4" class="cart_total"><span class="red">TOTAL + TAX:</span></td>
				<td>
					<asp:Label runat="server" ID="lTotalAndTax" />$</td>
			</tr>
		</table>
	</div>


	<div class="title">
		<span class="title_icon">
			<img src="images/bullet1.gif" alt="" title="" /></span>Shippment Address
	</div>

	<div class="feat_prod_box_details">
		<br />
		<table>
			<tr>
				<td>
					<label>Address:</label>
				</td>
				<td>
					<asp:Label runat="server" ID="lAddress"/>
				</td>
			</tr>
			<tr>
				<td>
					<label>Postal Code:</label>
				</td>
				<td>
					<asp:Label runat="server" ID="lPostalCode"/>
				</td>
			</tr>
			<tr>
				<td>
					<label>City:</label>
				</td>
				<td>
					<asp:Label runat="server" ID="lCity"/>
				</td>
			</tr>
			
			<tr>
				<td>
					<label>State:</label>
				</td>
				<td>
					<asp:Label runat="server" ID="lState"/>
				</td>
			</tr>
		</table>
	</div>

	<asp:Button runat="server" CssClass="register" Text="Continue Shopping" ID="bCancel" OnClick="bCancel_OnClick" CausesValidation="False" />

	<div class="clear"></div>

</asp:Content>