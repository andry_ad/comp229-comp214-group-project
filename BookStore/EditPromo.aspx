﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="EditPromo.aspx.cs" Inherits="BookStore.EditPromo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">

    <table>
        <tr>
            <td>
                <label class="contact"><strong>Code:</strong></label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbCode" CssClass="contact_input" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="tbCode" Display="None" ErrorMessage="'Code' field must be set" />
            </td>
        </tr>
        
        <tr>
            <td>
                <label class="contact"><strong>Expiry:</strong></label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbExpiry" CssClass="contact_input" />
                <asp:RequiredFieldValidator ID="vExpiry" runat="server" ControlToValidate="tbCode" Display="None" ErrorMessage="'Expiry' field must be set" />
            </td>
        </tr>
        
        <tr>
            <td>
                <label class="contact"><strong>Discount:</strong></label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbDiscount" CssClass="contact_input" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="tbCode" Display="None" ErrorMessage="'Discount' field must be set" />
                <asp:RangeValidator ID="vRDiscount" runat="server" ControlToValidate="tbDiscount" ErrorMessage="Invalid 'Discount'" Type="Double" Display="None" MinimumValue="0.01" MaximumValue="0.99" />
            </td>
        </tr>

        <tr>
            <td></td>
            <td>
                <asp:ValidationSummary runat="server" HeaderText="Please fix next error(s):" ShowMessageBox="False" DisplayMode="BulletList" ShowSummary="True" ForeColor="Red" />
            </td>
        </tr>

    </table>

    <asp:Button runat="server" CssClass="register" ID="bSave" OnClick="bSave_OnClick" />

</asp:Content>