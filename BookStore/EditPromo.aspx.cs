﻿using System;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using BookStore.App_Data;

namespace BookStore
{
	public partial class EditPromo : Page
	{
		private const string PromoId = "promoId";

		private const string VSIsNewPromo = "IsNewPromo";
		private const string VSPromoId = "PromoId";

		protected bool IsNewPromo
		{
			get { return (bool)ViewState[VSIsNewPromo]; }
			private set { ViewState[VSIsNewPromo] = value; }
		}

		protected int? EditPromoId
		{
			get { return (int?)ViewState[VSPromoId]; }
			private set { ViewState[VSPromoId] = value; }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!MySession.IsUserAdmin)
			{
				Response.Redirect("Login.aspx");
				return;
			}

			if (IsPostBack)
				return;

			vExpiry.InitialValue = CultureInfo.CurrentCulture.DateTimeFormat.FullDateTimePattern;
			tbExpiry.Text = DateTime.Now.AddDays(10).ToString();

			using (var c = new DatabaseContainer())
			{
				var sId = Request.QueryString[PromoId];
				IsNewPromo = string.IsNullOrWhiteSpace(sId);
				bSave.Text = IsNewPromo
								 ? "Add"
								 : "Update";

				if (!IsNewPromo)
				{
					int id;

					if (!int.TryParse(sId, out id))
					{
						Response.Redirect(BookStore.Error.GetErrorPageUrl("Invalid promo id"));
						return;
					}

					PROMOES_GET_BY_ID_Result book;

					try
					{
						book = c.PROMOES_GET_BY_ID(id).Single();
					}
					catch
					{
						Response.Redirect(BookStore.Error.GetErrorPageUrl("Invalid promo id"));
						return;
					}

					EditPromoId = id;

					tbCode.Text = book.Code;
					tbExpiry.Text = book.Expiry.ToString();
					tbDiscount.Text = book.Discount.ToString();
				}
			}
		}

		public static string GetUrlForPromoId(int id)
		{
			return string.Format("EditPromo.aspx?{0}={1}", PromoId, id);
		}

		protected void bSave_OnClick(object sender, EventArgs e)
		{
			if (!Page.IsValid)
				return;

			try
			{
				if (IsNewPromo)
				{
					//todo ad add exception handling
					using (var c = new DatabaseContainer())
					{
						var pId = new ObjectParameter("p_id", typeof(int));

						c.PROMOES_ADD(tbCode.Text,
							DateTime.Parse(tbExpiry.Text),
							1,
							decimal.Parse(tbDiscount.Text),
							pId);
					}
				}
				else
				{
					if (!EditPromoId.HasValue)
					{
						Response.Redirect(BookStore.Error.GetErrorPageUrl("Internal error. Cannot retreive promo info"));
						return;
					}

					using (var c = new DatabaseContainer())
					{
						//todo ad add exception handling
						c.PROMOES_UPDATE(EditPromoId, tbCode.Text,
							DateTime.Parse(tbExpiry.Text),
							decimal.Parse(tbDiscount.Text));
					}
				}
			}
			catch
			{
				Response.Redirect(BookStore.Error.GetErrorPageUrl("Failed to save promo"));
			}

			Response.Redirect("Promoes.aspx");
		}
	}
}