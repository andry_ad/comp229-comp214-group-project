﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using BookStore.App_Data;

namespace BookStore
{
	public partial class Cart : Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack)
				return;

			if (!MySession.IsUserLoged)
			{
				Response.Redirect("Index.aspx");
				return;
			}

			UpdateBooks();
		}

		private void UpdateBooks()
		{
			List<BASKET_GET_BY_USER_Result> books;
			try
			{
				CartDetails cart = null;
				using (var c = new DatabaseContainer())
				{
					books = c.BASKET_GET_BY_USER(MySession.UserOrAdminId).ToList();
					cart = BasketGetTotal(c, MySession.UserOrAdminId);
				}

				lTotal.Text = cart.TotalPrice.ToString();
				lTotalTax.Text = cart.TotalTax.ToString();
				lTotalAndTax.Text = cart.TotalGrand.ToString();
			}
			catch
			{
				Response.Redirect(BookStore.Error.GetErrorPageUrl("Failed to get books. Please try again later"));
				return;
			}

			rBooks.DataSource = books;
			rBooks.DataBind();
		}

		public static CartDetails BasketGetTotal(DatabaseContainer c, int id)
		{
			var total_qty = new ObjectParameter("total_qty", typeof (int));
			var total_price = new ObjectParameter("total_price", typeof (decimal));
			var total_tax = new ObjectParameter("total_tax", typeof (decimal));
			var total_grand = new ObjectParameter("total_grand", typeof (decimal));

			c.BASKET_GET_TOTAL(MySession.UserOrAdminId, total_qty, total_price, total_tax, total_grand);

			return new CartDetails((int)(decimal)total_qty.Value,
				(decimal)total_price.Value,
				(decimal)total_tax.Value,
				(decimal)total_grand.Value,
				0);
		}

		protected void rBooks_OnItemCommand(object source, RepeaterCommandEventArgs e)
		{
			if (e.CommandName == "Remove")
			{
				using (var c = new DatabaseContainer())
					c.BASKET_REMOVE(MySession.UserOrAdminId, decimal.Parse((string)e.CommandArgument));
				UpdateBooks();
			}
			else if (e.CommandName == "Update")
			{
				var id = int.Parse((string)e.CommandArgument);

				var tbQty = (TextBox)e.Item.FindControl("tbQty");

				var qty = int.Parse(tbQty.Text);

				using (var c = new DatabaseContainer())
					c.BASKET_CHANGE_QTY(MySession.UserOrAdminId, id, qty);
				UpdateBooks();
			}
		}

		protected void bCheckout_OnClick(object sender, EventArgs e)
		{
			if(rBooks.Items.Count == 0)
			{
				Helper.ShowPopUpMsg(Page, "No items in the cart");
				return;
			}

			Response.Redirect("Checkout.aspx");
		}

		protected void bCancel_OnClick(object sender, EventArgs e)
		{
			Response.Redirect("Index.aspx");
		}
	}
}