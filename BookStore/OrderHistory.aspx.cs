﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web.UI;
using BookStore.App_Data;

namespace BookStore
{
	public partial class OrderHistory : Page
	{
		private const string OrderId = "orderId";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack)
				return;

			if (!MySession.IsUserLoged)
			{
				Response.Redirect("Login.aspx");
				return;
			}

			var oId = Request.QueryString[OrderId];

			if (oId == null)
			{
				Response.Redirect(BookStore.Error.GetErrorPageUrl("Invalid order id"));
				return;
			}

			var id = int.Parse(oId);
			lOrderId.Text = oId;

			List<BASKET_GET_Result> books;
			try
			{
				CartDetails cart = null;
				App_Data.User user = null;
				using (var c = new DatabaseContainer())
				{
					books = c.BASKET_GET(id).ToList();

					var total_qty = new ObjectParameter("total_qty", typeof (int));
					var total_price = new ObjectParameter("total_price", typeof (decimal));
					var total_tax = new ObjectParameter("total_tax", typeof (decimal));
					var total_grand = new ObjectParameter("total_grand", typeof (decimal));
					var p_discount = new ObjectParameter("p_discount", typeof(decimal));

					c.BASKET_GET_TOTAL_BY_BASKETID(id, total_qty, total_price, total_tax, total_grand, p_discount);

					cart = new CartDetails((int)(decimal)total_qty.Value,
						(decimal)total_price.Value,
						(decimal)total_tax.Value,
						(decimal)total_grand.Value,
						(decimal)p_discount.Value);

					user = c.Users.Single(u => u.Id == MySession.UserOrAdminId);
				}

				lTotal.Text = cart.TotalPrice.ToString();
				lDiscount.Text = (cart.Discount*100).ToString();
				lTotalTax.Text = cart.TotalTax.ToString();
				lTotalAndTax.Text = cart.TotalGrand.ToString();

				lAddress.Text = user.Address;
				lCity.Text = user.City;
				lPostalCode.Text = user.PostalCode;
				lState.Text = user.State;
			}
			catch
			{
				Response.Redirect(BookStore.Error.GetErrorPageUrl("Failed to get books. Please try again later"));
				return;
			}

			rBooks.DataSource = books;
			rBooks.DataBind();
		}

		public static string GetOrderIdPageUrl(int id)
		{
			return string.Format("OrderHistory.aspx?{0}={1}", OrderId, id);
		}

		protected void bCancel_OnClick(object sender, EventArgs e)
		{
			Response.Redirect("Index.aspx");
		}
	}
}