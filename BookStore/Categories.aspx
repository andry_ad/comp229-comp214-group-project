﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Categories.aspx.cs" Inherits="BookStore.Categories" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
    <asp:ListBox runat="server" ID="lbCategories" SelectionMode="Single" OnSelectedIndexChanged="lbCategories_OnSelectedIndexChanged" AutoPostBack="True" Rows="15" />
    <br />
    <br />
    <asp:Label runat="server" Text="Name:" />
    <br />
    <asp:TextBox runat="server" ID="tbName" />
    <br />
    <asp:RequiredFieldValidator runat="server" ControlToValidate="tbName" ErrorMessage="Category is empty" ForeColor="Red" Display="Dynamic" />
    <asp:Label runat="server" ID="lStatus" ForeColor="Red" />
    <br />
    <br />
    <asp:Button runat="server" ID="bUpdate" Text="Update" CssClass="register" OnClick="bUpdate_OnClick" />
    <asp:Button runat="server" ID="bAdd" Text="Add" CssClass="register" OnClick="bAdd_OnClick" />
    <asp:Button runat="server" ID="bDelete" Text="Delete" CssClass="register" OnClick="bDelete_OnClick" CausesValidation="False" />
</asp:Content>
