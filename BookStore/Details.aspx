﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Details.aspx.cs" Inherits="BookStore.Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
	<div class="crumb_nav">
		<a href="index.aspx">home</a> &gt;&gt; <%= BookInfo.Title %>
           
	</div>
	<div class="title"><span class="title_icon">
		<img src="images/bullet1.gif" alt="" title="" /></span><%= BookInfo.Title %></div>

	<div class="feat_prod_box_details">

		<div class="prod_img">
				<img src="productImages/<%=BookInfo.ISBN %>.jpg" alt="" title="" border="0" class="normalThumb" />
			<br />
			<br />
		</div>

		<div class="prod_det_box">
			<div class="box_top"></div>
			<div class="box_center">
				<div class="prod_title">Details</div>
				<p class="details">
                    <%= BookInfo.Author %> 
				</p>
                <p class="details">
                     <%= BookInfo.Year.Year %> 
				</p>
                <p class="details">
                    <%= BookInfo.Publisher %>
				</p>
                <p class="details">
                    <%= BookInfo.ISBN %>
				</p>
				<div class="price"><strong>PRICE:</strong> <span class="red"><%= BookInfo.Price %> $</span></div>
				<div class="price">
					<strong>Categories:</strong>
                    
                   <% foreach (var c in BookCategories) { %>
                    <span><%= c.Name %></span>
                   <% } %>

				</div>
                
                <div class="price">
					<strong>Quantity:</strong>
                    <asp:TextBox runat="server" ID="tbQuantity" Text="1" Width="40" />
                    <br/>
                    <asp:RangeValidator runat="server" ControlToValidate="tbQuantity" MinimumValue="1" MaximumValue="100" Type="Integer" ErrorMessage="Invalid quantity" ForeColor="Red"/>
				</div>
                
			    <asp:Button runat="server" CssClass="register" Text="Cancel" ID="bContinue" OnClick="bContinue_OnClick"/>
			    <asp:Button runat="server" CssClass="register" Text="Add To Cart" ID="bAddToCart" OnClick="bAddToCart_OnClick"/>

				<div class="clear"></div>
			</div>

			<div class="box_bottom"></div>
		</div>
		<div class="clear"></div>
	</div>

	<div class="clear"></div>
</asp:Content>
