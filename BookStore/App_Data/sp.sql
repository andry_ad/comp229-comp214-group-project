--------------------------------------------------------
--  File created - Friday-April-15-2016   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure ADMIN_LOGIN
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."ADMIN_LOGIN" 
(
  p_email "Administrators"."Email"%TYPE,
  p_pass "Administrators"."Password"%TYPE,
  p_adminId OUT "Administrators"."Id"%TYPE
)
IS
BEGIN
  select "Id"
  into p_adminId
  from "Administrators" u
  where u."Email" = p_email AND u."Password" = p_pass;

EXCEPTION 
  WHEN NO_DATA_FOUND THEN
    p_adminId := -1;
END;

/
--------------------------------------------------------
--  DDL for Procedure BOOKS_ADD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."BOOKS_ADD" 
( 
  p_title "Books"."Title"%TYPE,
  p_isbn "Books"."ISBN"%TYPE,  
  p_author "Books"."Author"%TYPE,
  p_publisher "Books"."Publisher"%TYPE,
  p_year "Books"."Year"%TYPE,
  p_price "Books"."Price"%TYPE,
  p_searchCounts "Books"."SearchCounts"%TYPE
)
IS
BEGIN
  Insert into BOOKSTORE."Books" ("Id","Title",ISBN,"Author","Publisher","Year","Price","SearchCounts") 
  values (SEQ_BOOKS_ID.NEXTVAL, p_title, p_isbn, p_author, p_publisher, p_year, p_price, p_searchCounts);
END;

/
--------------------------------------------------------
--  DDL for Procedure BOOKS_GET
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."BOOKS_GET" 
(p_result OUT SYS_REFCURSOR)
IS
BEGIN
  OPEN p_result FOR select * from "Books" order by "Books"."Id";
END;

/
--------------------------------------------------------
--  DDL for Procedure BOOKS_GET_BY_ID
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."BOOKS_GET_BY_ID" 
(
p_id "Books"."Id"%TYPE, 
p_result OUT SYS_REFCURSOR)
IS

BEGIN
  
  OPEN p_result FOR select * from "Books" where "Id" = p_id;
  
END;

/
--------------------------------------------------------
--  DDL for Procedure BOOKS_GETSHORTDESCRIPTION
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."BOOKS_GETSHORTDESCRIPTION" 
(
p_categoryId "Categories"."Id"%TYPE, 
p_result OUT SYS_REFCURSOR)
IS

BEGIN

  IF p_categoryId <= 0 THEN
  -- get all books
      OPEN p_result FOR select "Id", "Title", "ISBN"  from "Books" b;
  ELSE
  -- get books for specific category
      OPEN p_result FOR select "Id", "Title", "ISBN"  
        from "Books"
        where "Books"."Id" in (select "Books_Id" from "BookCategory" where "Categories_Id" = p_categoryId);
  END IF;
  
END;

/
--------------------------------------------------------
--  DDL for Procedure BOOKS_UPDATE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."BOOKS_UPDATE" 
( 
  p_id "Books"."Id"%TYPE,
  p_title "Books"."Title"%TYPE,
  p_isbn "Books"."ISBN"%TYPE,  
  p_author "Books"."Author"%TYPE,
  p_publisher "Books"."Publisher"%TYPE,
  p_year "Books"."Year"%TYPE,
  p_price "Books"."Price"%TYPE,
  p_searchCounts "Books"."SearchCounts"%TYPE
)
IS
BEGIN

  update "Books"
  set "Title" = p_title,
  "ISBN" = p_isbn,
  "Author" = p_author,
  "Publisher" = p_publisher,
  "Year" = p_year,
  "Price"= p_price,
  "SearchCounts" = p_searchCounts
  where "Id" = p_id;
  
  IF SQL%ROWCOUNT = 0 THEN
      raise_application_error(-20004, 'Invalid book id');
  END IF;

END;

/
--------------------------------------------------------
--  DDL for Procedure CATEGORIES_GET
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."CATEGORIES_GET" 
(p_result OUT SYS_REFCURSOR)
IS

BEGIN
  OPEN p_result FOR select *
        from "Categories" c
        order by c."Name";
END;

/
--------------------------------------------------------
--  DDL for Procedure CATEGORIES_GET_FOR_BOOK
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."CATEGORIES_GET_FOR_BOOK" 
(
  p_bookId "Books"."Id"%TYPE,
  result OUT SYS_REFCURSOR
)
IS
BEGIN
  OPEN result FOR select c."Id", c."Name" 
    from "BookCategory" bc
    inner join "Categories" c
    on c."Id" = bc."Categories_Id"
    where bc."Books_Id" = p_bookId
    order by c."Name";
END;

/
--------------------------------------------------------
--  DDL for Procedure CATEGORIES_GET_NAME_BY_ID
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."CATEGORIES_GET_NAME_BY_ID" 
(
  p_id "Categories"."Id"%TYPE,
  p_name OUT "Categories"."Name"%TYPE
)
IS
BEGIN
  select c."Name"
  into p_name
  from "Categories" c
  where c."Id" = p_id;
  
EXCEPTION
  when NO_DATA_FOUND then
    raise_application_error(-20003, 'Invalid category id');
END;

/
--------------------------------------------------------
--  DDL for Procedure CATEGORIES_GET_WITHBOOKS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."CATEGORIES_GET_WITHBOOKS" 
(p_result OUT SYS_REFCURSOR)
IS

BEGIN
  OPEN p_result FOR select *
        from "Categories" c
        where c."Id" in (select distinct "Categories_Id" from "BookCategory")
        order by c."Name";
END;

/
--------------------------------------------------------
--  DDL for Procedure CATEGORIES_SET_FOR_BOOK
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."CATEGORIES_SET_FOR_BOOK" 
(
  p_bookId "Books"."Id"%TYPE,
  p_categoryId "Categories"."Id"%TYPE
)
IS
  lv_exists number(10);
BEGIN
  select count(*)
  into lv_exists
  from "BookCategory";
  
  IF lv_exists > 0 THEN
    RETURN;
  END IF;

  Insert into BOOKSTORE."BookCategory" ("Books_Id","Categories_Id") 
  values (p_bookId, p_categoryId);
END;

/
--------------------------------------------------------
--  DDL for Procedure CHECK_UNQIUE_EMAIL
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."CHECK_UNQIUE_EMAIL" 
(
  p_email "Users"."Email"%TYPE,
  p_isUnique OUT NUMBER --cannot use boolean because odp.net provider doesn't support it properly
)
IS
  lv_count NUMBER(20);
BEGIN
  --check user table
  select count(*)
  into lv_count
  from "Users" u
  where u."Email" = p_email;
  
  IF lv_count > 0 THEN
    p_isUnique := 0;
    return;
  END IF;
  
  --check admin table
  select count(*)
  into lv_count
  from "Administrators" a
  where a."Email" = p_email;
  
  IF lv_count > 0 THEN
    p_isUnique := 0;
    return;
  END IF;
  
  p_isUnique := 1;
END;

/
--------------------------------------------------------
--  DDL for Procedure USERS_GET
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."USERS_GET" 
(p_result OUT SYS_REFCURSOR)
IS
BEGIN
  OPEN p_result FOR select * from "Users" order by "Users"."Id";
END;

/
--------------------------------------------------------
--  DDL for Procedure USERS_LOGIN
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."USERS_LOGIN" 
(
  p_email "Users"."Email"%TYPE,
  p_pass "Users"."Password"%TYPE,
  p_userId OUT "Users"."Id"%TYPE
)
IS
BEGIN
  select "Id"
  into p_userId
  from "Users" u
  where u."Email" = p_email AND u."Password" = p_pass;

EXCEPTION 
  WHEN NO_DATA_FOUND THEN
  p_userId := -1;
END;

/
--------------------------------------------------------
--  DDL for Procedure USERS_REGISTER
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."USERS_REGISTER" 
(
  p_email "Users"."Email"%TYPE,
  p_password "Users"."Password"%TYPE,
  p_firstName "Users"."FirstName"%TYPE,
  p_lastName "Users"."LastName"%TYPE,
  p_birthday "Users"."Birhday"%TYPE,
  p_dateOfRegistration "Users"."DateOfRegistration"%TYPE,
  p_phone "Users"."PhoneNumber"%TYPE,
  p_address "Users"."Address"%TYPE,
  p_postalCode "Users"."PostalCode"%TYPE,
  p_city "Users"."City"%TYPE,
  p_state "Users"."State"%TYPE
)
IS
  lv_isEmailUnique NUMBER(1);
BEGIN
  -- validation
  CHECK_UNQIUE_EMAIL(p_email, lv_isEmailUnique);
  
  IF lv_isEmailUnique = 0 THEN
    raise_application_error(-20001, 'Email is not unique');
    RETURN;
  END IF;
  
  -- register
  Insert into BOOKSTORE."Users" ("Id","Email","Password","FirstName","LastName","Birhday","DateOfRegistration","PhoneNumber","Address","PostalCode","City","State") 
  values (SEQ_USERS_ID.NEXTVAL, p_email, p_password, p_firstName, p_lastName, p_birthday, p_dateOfRegistration, p_phone, p_address, p_postalCode, p_city, p_state);
  
END;

/
