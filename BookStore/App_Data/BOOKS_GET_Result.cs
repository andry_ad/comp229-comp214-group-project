//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BookStore.App_Data
{
    using System;
    
    public partial class BOOKS_GET_Result
    {
        public decimal Id { get; set; }
        public string Title { get; set; }
        public string ISBN { get; set; }
        public string Author { get; set; }
        public string Publisher { get; set; }
        public System.DateTime Year { get; set; }
        public decimal Price { get; set; }
        public decimal SearchCounts { get; set; }
    }
}
