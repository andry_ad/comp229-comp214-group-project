--------------------------------------------------------
--  File created - Wednesday-April-20-2016   
--------------------------------------------------------
DROP TABLE "BOOKSTORE"."Administrators" cascade constraints;
DROP TABLE "BOOKSTORE"."BasketItems" cascade constraints;
DROP TABLE "BOOKSTORE"."Baskets" cascade constraints;
DROP TABLE "BOOKSTORE"."BookCategory" cascade constraints;
DROP TABLE "BOOKSTORE"."Books" cascade constraints;
DROP TABLE "BOOKSTORE"."Categories" cascade constraints;
DROP TABLE "BOOKSTORE"."Logs" cascade constraints;
DROP TABLE "BOOKSTORE"."Promoes" cascade constraints;
DROP TABLE "BOOKSTORE"."Users" cascade constraints;
DROP SEQUENCE "BOOKSTORE"."SEQ_BOOKS_ID";
DROP SEQUENCE "BOOKSTORE"."SEQ_CATEGORY_ID";
DROP SEQUENCE "BOOKSTORE"."SEQ_USERS_ID";
DROP PACKAGE "BOOKSTORE"."CONSTANTS";
DROP PACKAGE "BOOKSTORE"."EXCEPTIONS";
DROP PROCEDURE "BOOKSTORE"."ADMIN_LOGIN";
DROP PROCEDURE "BOOKSTORE"."BASKET_ADD";
DROP PROCEDURE "BOOKSTORE"."BASKET_APPLY_PROMO";
DROP PROCEDURE "BOOKSTORE"."BASKET_CHANGE_QTY";
DROP PROCEDURE "BOOKSTORE"."BASKET_CHECKOUT";
DROP PROCEDURE "BOOKSTORE"."BASKET_GET_BY_USER";
DROP PROCEDURE "BOOKSTORE"."BASKET_GET_TOTAL";
DROP PROCEDURE "BOOKSTORE"."BASKET_REMOVE";
DROP PROCEDURE "BOOKSTORE"."BOOKS_ADD";
DROP PROCEDURE "BOOKSTORE"."BOOKS_GET";
DROP PROCEDURE "BOOKSTORE"."BOOKS_GET_BY_ID";
DROP PROCEDURE "BOOKSTORE"."BOOKS_GETSHORTDESCRIPTION";
DROP PROCEDURE "BOOKSTORE"."BOOKS_IS_ISBN_UNIQUE";
DROP PROCEDURE "BOOKSTORE"."BOOKS_UPDATE";
DROP PROCEDURE "BOOKSTORE"."CATEGORIES_ADD";
DROP PROCEDURE "BOOKSTORE"."CATEGORIES_BOOK_ADD";
DROP PROCEDURE "BOOKSTORE"."CATEGORIES_BOOK_CLEAR";
DROP PROCEDURE "BOOKSTORE"."CATEGORIES_DELETE";
DROP PROCEDURE "BOOKSTORE"."CATEGORIES_GET";
DROP PROCEDURE "BOOKSTORE"."CATEGORIES_GET_FOR_BOOK";
DROP PROCEDURE "BOOKSTORE"."CATEGORIES_GET_NAME_BY_ID";
DROP PROCEDURE "BOOKSTORE"."CATEGORIES_GET_WITHBOOKS";
DROP PROCEDURE "BOOKSTORE"."CATEGORIES_IS_NAME_UNIQUE";
DROP PROCEDURE "BOOKSTORE"."CATEGORIES_SET_FOR_BOOK";
DROP PROCEDURE "BOOKSTORE"."CATEGORIES_UPDATE";
DROP PROCEDURE "BOOKSTORE"."CHECK_BOOKID_EXIST";
DROP PROCEDURE "BOOKSTORE"."CHECK_UNQIUE_EMAIL";
DROP PROCEDURE "BOOKSTORE"."CHECK_USERID_EXIST";
DROP PROCEDURE "BOOKSTORE"."CREATE_BASKET";
DROP PROCEDURE "BOOKSTORE"."PROMOES_GET_DETAILS";
DROP PROCEDURE "BOOKSTORE"."TEST";
DROP PROCEDURE "BOOKSTORE"."USERS_GET";
DROP PROCEDURE "BOOKSTORE"."USERS_LOGIN";
DROP PROCEDURE "BOOKSTORE"."USERS_REGISTER";
--------------------------------------------------------
--  DDL for Sequence SEQ_BOOKS_ID
--------------------------------------------------------

   CREATE SEQUENCE  "BOOKSTORE"."SEQ_BOOKS_ID"  MINVALUE 1 MAXVALUE 9999999999999 INCREMENT BY 1 START WITH 120 CACHE 20 ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEQ_CATEGORY_ID
--------------------------------------------------------

   CREATE SEQUENCE  "BOOKSTORE"."SEQ_CATEGORY_ID"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 100 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEQ_USERS_ID
--------------------------------------------------------

   CREATE SEQUENCE  "BOOKSTORE"."SEQ_USERS_ID"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 100 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table Administrators
--------------------------------------------------------

  CREATE TABLE "BOOKSTORE"."Administrators" 
   (	"Id" NUMBER(10,0), 
	"Email" NVARCHAR2(100), 
	"Password" NVARCHAR2(50)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table BasketItems
--------------------------------------------------------

  CREATE TABLE "BOOKSTORE"."BasketItems" 
   (	"Id" NUMBER(10,0), 
	"Quantity" NUMBER(10,0), 
	"BasketId" NUMBER(10,0), 
	"BookId" NUMBER(10,0), 
	"PRICE" NUMBER(36,2)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table Baskets
--------------------------------------------------------

  CREATE TABLE "BOOKSTORE"."Baskets" 
   (	"Id" NUMBER(10,0), 
	"DateFinished" DATE, 
	"PromoId" NUMBER(10,0), 
	"UserId" NUMBER(10,0), 
	"DISCOUNT" NUMBER(36,2)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table BookCategory
--------------------------------------------------------

  CREATE TABLE "BOOKSTORE"."BookCategory" 
   (	"Books_Id" NUMBER(10,0), 
	"Categories_Id" NUMBER(10,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table Books
--------------------------------------------------------

  CREATE TABLE "BOOKSTORE"."Books" 
   (	"Id" NUMBER(10,0), 
	"Title" NVARCHAR2(255), 
	"ISBN" NVARCHAR2(20), 
	"Author" NVARCHAR2(100), 
	"Publisher" NVARCHAR2(100), 
	"Year" DATE, 
	"Price" NUMBER(36,2), 
	"SearchCounts" NUMBER(10,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table Categories
--------------------------------------------------------

  CREATE TABLE "BOOKSTORE"."Categories" 
   (	"Id" NUMBER(10,0), 
	"Name" NVARCHAR2(100)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table Logs
--------------------------------------------------------

  CREATE TABLE "BOOKSTORE"."Logs" 
   (	"Id" NUMBER(10,0), 
	"Date" DATE, 
	"Title" NVARCHAR2(100), 
	"Description" NVARCHAR2(255), 
	"User" NUMBER(10,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table Promoes
--------------------------------------------------------

  CREATE TABLE "BOOKSTORE"."Promoes" 
   (	"Id" NUMBER(10,0), 
	"Code" NVARCHAR2(20), 
	"Expiry" DATE, 
	"AdministratorId" NUMBER(10,0), 
	"Discount" NUMBER(36,2)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table Users
--------------------------------------------------------

  CREATE TABLE "BOOKSTORE"."Users" 
   (	"Id" NUMBER(10,0), 
	"Email" NVARCHAR2(100), 
	"Password" NVARCHAR2(50), 
	"FirstName" NVARCHAR2(100), 
	"LastName" NVARCHAR2(100), 
	"Birhday" DATE, 
	"DateOfRegistration" DATE, 
	"PhoneNumber" NVARCHAR2(100), 
	"Address" NVARCHAR2(250), 
	"PostalCode" NVARCHAR2(20), 
	"City" NVARCHAR2(100), 
	"State" NVARCHAR2(50)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
REM INSERTING into BOOKSTORE."Administrators"
SET DEFINE OFF;
Insert into BOOKSTORE."Administrators" ("Id","Email","Password") values (1,'admin@a.com','admin');
REM INSERTING into BOOKSTORE."BasketItems"
SET DEFINE OFF;
REM INSERTING into BOOKSTORE."Baskets"
SET DEFINE OFF;
REM INSERTING into BOOKSTORE."BookCategory"
SET DEFINE OFF;
Insert into BOOKSTORE."BookCategory" ("Books_Id","Categories_Id") values (22,1);
Insert into BOOKSTORE."BookCategory" ("Books_Id","Categories_Id") values (23,1);
Insert into BOOKSTORE."BookCategory" ("Books_Id","Categories_Id") values (24,1);
Insert into BOOKSTORE."BookCategory" ("Books_Id","Categories_Id") values (25,1);
Insert into BOOKSTORE."BookCategory" ("Books_Id","Categories_Id") values (26,3);
Insert into BOOKSTORE."BookCategory" ("Books_Id","Categories_Id") values (27,3);
Insert into BOOKSTORE."BookCategory" ("Books_Id","Categories_Id") values (28,3);
Insert into BOOKSTORE."BookCategory" ("Books_Id","Categories_Id") values (29,3);
Insert into BOOKSTORE."BookCategory" ("Books_Id","Categories_Id") values (30,3);
Insert into BOOKSTORE."BookCategory" ("Books_Id","Categories_Id") values (31,5);
Insert into BOOKSTORE."BookCategory" ("Books_Id","Categories_Id") values (32,5);
Insert into BOOKSTORE."BookCategory" ("Books_Id","Categories_Id") values (33,5);
Insert into BOOKSTORE."BookCategory" ("Books_Id","Categories_Id") values (34,5);
Insert into BOOKSTORE."BookCategory" ("Books_Id","Categories_Id") values (35,6);
Insert into BOOKSTORE."BookCategory" ("Books_Id","Categories_Id") values (36,6);
Insert into BOOKSTORE."BookCategory" ("Books_Id","Categories_Id") values (37,6);
Insert into BOOKSTORE."BookCategory" ("Books_Id","Categories_Id") values (41,4);
Insert into BOOKSTORE."BookCategory" ("Books_Id","Categories_Id") values (42,4);
Insert into BOOKSTORE."BookCategory" ("Books_Id","Categories_Id") values (43,4);
Insert into BOOKSTORE."BookCategory" ("Books_Id","Categories_Id") values (44,4);
Insert into BOOKSTORE."BookCategory" ("Books_Id","Categories_Id") values (45,4);
REM INSERTING into BOOKSTORE."Books"
SET DEFINE OFF;
Insert into BOOKSTORE."Books" ("Id","Title",ISBN,"Author","Publisher","Year","Price","SearchCounts") values (41,'Mastering Xamarin Forms','978-1-78528-494-6','Ed Sidner','Packt Publishing',to_date('30-JAN-2016','DD-MON-YYYY'),27.99,4);
Insert into BOOKSTORE."Books" ("Id","Title",ISBN,"Author","Publisher","Year","Price","SearchCounts") values (42,'Mobile Game Development With Unity','978-1-4919-2234-7','Jon Manning and Paris Buttfield Addison ','O Reilly Media',to_date('25-JUL-2015','DD-MON-YYYY'),47.99,0);
Insert into BOOKSTORE."Books" ("Id","Title",ISBN,"Author","Publisher","Year","Price","SearchCounts") values (43,'Applying Apache Cordova And Phone gap','978-1-2585-369-8','Maximilano Firtman','O Reilly media',to_date('09-DEC-2015','DD-MON-YYYY'),96.99,3);
Insert into BOOKSTORE."Books" ("Id","Title",ISBN,"Author","Publisher","Year","Price","SearchCounts") values (44,'Tragic Design','978-1-4919-2354-2','Jonathan Shariat','O Reilly Media',to_date('25-DEC-2015','DD-MON-YYYY'),42.99,6);
Insert into BOOKSTORE."Books" ("Id","Title",ISBN,"Author","Publisher","Year","Price","SearchCounts") values (45,'Responsive Web Design With AngularJS','978-1-78439-572-8','Sandeep Kumar Patel','Packt Publishing',to_date('01-JAN-2014','DD-MON-YYYY'),33.99,1);
Insert into BOOKSTORE."Books" ("Id","Title",ISBN,"Author","Publisher","Year","Price","SearchCounts") values (22,'Anroid Application Development Cook Book','978-1-78588-920-2','Rick Boyer and Kyle Mew','Packt Publishing',to_date('04-JAN-2013','DD-MON-YYYY'),37.99,2);
Insert into BOOKSTORE."Books" ("Id","Title",ISBN,"Author","Publisher","Year","Price","SearchCounts") values (23,'Open CV Anroid Programming by Example','978-1-78528-293-5','Amgad Muhammad','Packt Publishing',to_date('01-DEC-2015','DD-MON-YYYY'),24.99,3);
Insert into BOOKSTORE."Books" ("Id","Title",ISBN,"Author","Publisher","Year","Price","SearchCounts") values (24,'Mastering Anroid Application Development','978-1-78588-762-8','Antonio Pachon Ruiz','Packt Publishing',to_date('01-OCT-2015','DD-MON-YYYY'),37.99,2);
Insert into BOOKSTORE."Books" ("Id","Title",ISBN,"Author","Publisher","Year","Price","SearchCounts") values (25,'High Performance Anroid Apps','978-1-4919-1249-2','Doug Sillars','O Reilly media',to_date('25-JUN-2015','DD-MON-YYYY'),44.99,2);
Insert into BOOKSTORE."Books" ("Id","Title",ISBN,"Author","Publisher","Year","Price","SearchCounts") values (26,'IOS 9 SDK Development','978-1-68050-132-2','Chris Adamson and Janie Clayton','Pragmatic BookShelf',to_date('27-MAR-2016','DD-MON-YYYY'),44.99,5);
Insert into BOOKSTORE."Books" ("Id","Title",ISBN,"Author","Publisher","Year","Price","SearchCounts") values (27,'Protocol-Oriented Programming with Swift','978-1-78588-634-8','Jon Hoffmann','Packt Publishing',to_date('23-FEB-2016','DD-MON-YYYY'),33.99,2);
Insert into BOOKSTORE."Books" ("Id","Title",ISBN,"Author","Publisher","Year","Price","SearchCounts") values (28,'IOS Application Security','978-1-59327-754-3','David Theil','No Starch Press',to_date('25-MAY-2015','DD-MON-YYYY'),55.99,1);
Insert into BOOKSTORE."Books" ("Id","Title",ISBN,"Author","Publisher","Year","Price","SearchCounts") values (29,'Learning IOS penetration Testing','978-1-78588-679-9','Swaroop Yermalkar','Packt Publishing',to_date('07-JAN-2016','DD-MON-YYYY'),36.99,0);
Insert into BOOKSTORE."Books" ("Id","Title",ISBN,"Author","Publisher","Year","Price","SearchCounts") values (30,'Mastering IOS Game Development','978-1-78355-436-2','Miguel DeQuadros','Packt Publishing',to_date('29-DEC-2015','DD-MON-YYYY'),52.99,6);
Insert into BOOKSTORE."Books" ("Id","Title",ISBN,"Author","Publisher","Year","Price","SearchCounts") values (31,'Designing Machine Learning Systems With Python','978-1-78588-078-0','David Julian','Packt Publishing',to_date('26-JUL-2013','DD-MON-YYYY'),36.99,4);
Insert into BOOKSTORE."Books" ("Id","Title",ISBN,"Author","Publisher","Year","Price","SearchCounts") values (32,'Learning Python Design Patterns','978-1-78588-737-6','Chetan Giridhar','Packt Publishing',to_date('01-JAN-2013','DD-MON-YYYY'),42.99,6);
Insert into BOOKSTORE."Books" ("Id","Title",ISBN,"Author","Publisher","Year","Price","SearchCounts") values (33,'Learning Scrappy','978-1-78439-091-4','Dimitrios Kouzis Loukas','Packt Publishing',to_date('18-MAR-2013','DD-MON-YYYY'),29.99,5);
Insert into BOOKSTORE."Books" ("Id","Title",ISBN,"Author","Publisher","Year","Price","SearchCounts") values (34,'Data Visualization with Python and JavaScript','978-1-65485-001-8','O Reilly Media','O Reilly Media',to_date('25-JUN-2016','DD-MON-YYYY'),66.99,3);
Insert into BOOKSTORE."Books" ("Id","Title",ISBN,"Author","Publisher","Year","Price","SearchCounts") values (35,'Testing with Junit','978-1-78216-661-0','Frank Appel','Packt Publishing',to_date('23-APR-2013','DD-MON-YYYY'),25.99,2);
Insert into BOOKSTORE."Books" ("Id","Title",ISBN,"Author","Publisher","Year","Price","SearchCounts") values (36,'Your Code as a Crime Scene','978-1-68050-038-7','Adam Tornhill','Pragmatic Bookshelf',to_date('25-FEB-2015','DD-MON-YYYY'),37.99,2);
Insert into BOOKSTORE."Books" ("Id","Title",ISBN,"Author","Publisher","Year","Price","SearchCounts") values (37,'The art of Application Performance Testing','978-1-4919-0049-9','Ian Molyeaux','O Reilly Media',to_date('01-JAN-2009','DD-MON-YYYY'),47.99,2);
REM INSERTING into BOOKSTORE."Categories"
SET DEFINE OFF;
Insert into BOOKSTORE."Categories" ("Id","Name") values (12,'Architechure');
Insert into BOOKSTORE."Categories" ("Id","Name") values (13,'Java');
Insert into BOOKSTORE."Categories" ("Id","Name") values (14,'.NET');
Insert into BOOKSTORE."Categories" ("Id","Name") values (15,'Some');
Insert into BOOKSTORE."Categories" ("Id","Name") values (16,'.NET1');
Insert into BOOKSTORE."Categories" ("Id","Name") values (1,'Anroid Programming');
Insert into BOOKSTORE."Categories" ("Id","Name") values (3,'IOS Programming');
Insert into BOOKSTORE."Categories" ("Id","Name") values (4,'Mobile Design And Development');
Insert into BOOKSTORE."Categories" ("Id","Name") values (5,'Python Programming');
Insert into BOOKSTORE."Categories" ("Id","Name") values (6,'Testing');
REM INSERTING into BOOKSTORE."Logs"
SET DEFINE OFF;
REM INSERTING into BOOKSTORE."Promoes"
SET DEFINE OFF;
Insert into BOOKSTORE."Promoes" ("Id","Code","Expiry","AdministratorId","Discount") values (2,'some',to_date('23-APR-2016','DD-MON-YYYY'),1,0.23);
REM INSERTING into BOOKSTORE."Users"
SET DEFINE OFF;
Insert into BOOKSTORE."Users" ("Id","Email","Password","FirstName","LastName","Birhday","DateOfRegistration","PhoneNumber","Address","PostalCode","City","State") values (1,'a@a.com','pass','f','l',to_date('04-APR-2016','DD-MON-YYYY'),to_date('05-APR-2016','DD-MON-YYYY'),'12341234','asdfasdf','l5a2w3','Mississauga','on');
Insert into BOOKSTORE."Users" ("Id","Email","Password","FirstName","LastName","Birhday","DateOfRegistration","PhoneNumber","Address","PostalCode","City","State") values (3,'andryakaad@gmail.com','3123','aasdf asdf','fasdf asdf',to_date('10-FEB-1990','DD-MON-YYYY'),to_date('13-APR-2016','DD-MON-YYYY'),'412341','asdf 1','a0a0a0','asdfas','Ontario');
Insert into BOOKSTORE."Users" ("Id","Email","Password","FirstName","LastName","Birhday","DateOfRegistration","PhoneNumber","Address","PostalCode","City","State") values (4,'andryakaad@gmail.com11','asdfasdf','asdf asd','fasdf asdf',to_date('10-FEB-1990','DD-MON-YYYY'),to_date('13-APR-2016','DD-MON-YYYY'),'432342','asdfa 3','a0a0a0','asdfas','Ontario');
--------------------------------------------------------
--  DDL for Index IX_FK_BookCategory_Category
--------------------------------------------------------

  CREATE INDEX "BOOKSTORE"."IX_FK_BookCategory_Category" ON "BOOKSTORE"."BookCategory" ("Categories_Id") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index PK_Administrators
--------------------------------------------------------

  CREATE UNIQUE INDEX "BOOKSTORE"."PK_Administrators" ON "BOOKSTORE"."Administrators" ("Id") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index CK_ISBN
--------------------------------------------------------

  CREATE UNIQUE INDEX "BOOKSTORE"."CK_ISBN" ON "BOOKSTORE"."Books" ("ISBN") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index PK_Users
--------------------------------------------------------

  CREATE UNIQUE INDEX "BOOKSTORE"."PK_Users" ON "BOOKSTORE"."Users" ("Id") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index IX_FK_BasketBasketItem
--------------------------------------------------------

  CREATE INDEX "BOOKSTORE"."IX_FK_BasketBasketItem" ON "BOOKSTORE"."BasketItems" ("BasketId") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index PK_Logs
--------------------------------------------------------

  CREATE UNIQUE INDEX "BOOKSTORE"."PK_Logs" ON "BOOKSTORE"."Logs" ("Id") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index PK_Baskets
--------------------------------------------------------

  CREATE UNIQUE INDEX "BOOKSTORE"."PK_Baskets" ON "BOOKSTORE"."Baskets" ("Id") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index IX_FK_BasketPromo
--------------------------------------------------------

  CREATE INDEX "BOOKSTORE"."IX_FK_BasketPromo" ON "BOOKSTORE"."Baskets" ("PromoId") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index PK_Books
--------------------------------------------------------

  CREATE UNIQUE INDEX "BOOKSTORE"."PK_Books" ON "BOOKSTORE"."Books" ("Id") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index PK_Categories
--------------------------------------------------------

  CREATE UNIQUE INDEX "BOOKSTORE"."PK_Categories" ON "BOOKSTORE"."Categories" ("Id") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index PK_BasketItems
--------------------------------------------------------

  CREATE UNIQUE INDEX "BOOKSTORE"."PK_BasketItems" ON "BOOKSTORE"."BasketItems" ("Id") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index IX_FK_UserBasket
--------------------------------------------------------

  CREATE INDEX "BOOKSTORE"."IX_FK_UserBasket" ON "BOOKSTORE"."Baskets" ("UserId") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index PK_BookCategory
--------------------------------------------------------

  CREATE UNIQUE INDEX "BOOKSTORE"."PK_BookCategory" ON "BOOKSTORE"."BookCategory" ("Books_Id", "Categories_Id") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index IX_FK_BookCategory_Book
--------------------------------------------------------

  CREATE INDEX "BOOKSTORE"."IX_FK_BookCategory_Book" ON "BOOKSTORE"."BookCategory" ("Books_Id") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index IX_FK_BookBasketItem
--------------------------------------------------------

  CREATE INDEX "BOOKSTORE"."IX_FK_BookBasketItem" ON "BOOKSTORE"."BasketItems" ("BookId") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index PK_Promoes
--------------------------------------------------------

  CREATE UNIQUE INDEX "BOOKSTORE"."PK_Promoes" ON "BOOKSTORE"."Promoes" ("Id") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index IX_FK_AdministratorPromo
--------------------------------------------------------

  CREATE INDEX "BOOKSTORE"."IX_FK_AdministratorPromo" ON "BOOKSTORE"."Promoes" ("AdministratorId") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  Constraints for Table Categories
--------------------------------------------------------

  ALTER TABLE "BOOKSTORE"."Categories" MODIFY ("Id" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Categories" MODIFY ("Name" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Categories" ADD CONSTRAINT "PK_Categories" PRIMARY KEY ("Id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
--------------------------------------------------------
--  Constraints for Table Users
--------------------------------------------------------

  ALTER TABLE "BOOKSTORE"."Users" MODIFY ("Id" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Users" MODIFY ("Email" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Users" MODIFY ("Password" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Users" MODIFY ("FirstName" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Users" MODIFY ("LastName" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Users" MODIFY ("Birhday" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Users" MODIFY ("DateOfRegistration" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Users" MODIFY ("PhoneNumber" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Users" MODIFY ("Address" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Users" MODIFY ("PostalCode" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Users" MODIFY ("City" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Users" MODIFY ("State" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Users" ADD CONSTRAINT "PK_Users" PRIMARY KEY ("Id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
--------------------------------------------------------
--  Constraints for Table BasketItems
--------------------------------------------------------

  ALTER TABLE "BOOKSTORE"."BasketItems" MODIFY ("Id" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."BasketItems" MODIFY ("Quantity" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."BasketItems" MODIFY ("BasketId" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."BasketItems" MODIFY ("BookId" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."BasketItems" ADD CONSTRAINT "PK_BasketItems" PRIMARY KEY ("Id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "BOOKSTORE"."BasketItems" MODIFY ("PRICE" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table Logs
--------------------------------------------------------

  ALTER TABLE "BOOKSTORE"."Logs" MODIFY ("Id" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Logs" MODIFY ("Date" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Logs" MODIFY ("Title" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Logs" MODIFY ("Description" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Logs" MODIFY ("User" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Logs" ADD CONSTRAINT "PK_Logs" PRIMARY KEY ("Id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
--------------------------------------------------------
--  Constraints for Table BookCategory
--------------------------------------------------------

  ALTER TABLE "BOOKSTORE"."BookCategory" MODIFY ("Books_Id" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."BookCategory" MODIFY ("Categories_Id" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."BookCategory" ADD CONSTRAINT "PK_BookCategory" PRIMARY KEY ("Books_Id", "Categories_Id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
--------------------------------------------------------
--  Constraints for Table Baskets
--------------------------------------------------------

  ALTER TABLE "BOOKSTORE"."Baskets" MODIFY ("Id" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Baskets" ADD CONSTRAINT "PK_Baskets" PRIMARY KEY ("Id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
--------------------------------------------------------
--  Constraints for Table Promoes
--------------------------------------------------------

  ALTER TABLE "BOOKSTORE"."Promoes" MODIFY ("Id" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Promoes" MODIFY ("Code" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Promoes" MODIFY ("Expiry" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Promoes" MODIFY ("AdministratorId" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Promoes" MODIFY ("Discount" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Promoes" ADD CONSTRAINT "PK_Promoes" PRIMARY KEY ("Id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
--------------------------------------------------------
--  Constraints for Table Administrators
--------------------------------------------------------

  ALTER TABLE "BOOKSTORE"."Administrators" MODIFY ("Id" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Administrators" MODIFY ("Email" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Administrators" MODIFY ("Password" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Administrators" ADD CONSTRAINT "PK_Administrators" PRIMARY KEY ("Id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
--------------------------------------------------------
--  Constraints for Table Books
--------------------------------------------------------

  ALTER TABLE "BOOKSTORE"."Books" ADD CONSTRAINT "CK_ISBN" UNIQUE ("ISBN")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "BOOKSTORE"."Books" MODIFY ("Id" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Books" MODIFY ("Title" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Books" MODIFY ("ISBN" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Books" MODIFY ("Author" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Books" MODIFY ("Publisher" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Books" MODIFY ("Year" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Books" MODIFY ("Price" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Books" MODIFY ("SearchCounts" NOT NULL ENABLE);
  ALTER TABLE "BOOKSTORE"."Books" ADD CONSTRAINT "PK_Books" PRIMARY KEY ("Id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table BasketItems
--------------------------------------------------------

  ALTER TABLE "BOOKSTORE"."BasketItems" ADD CONSTRAINT "FK_BasketBasketItem" FOREIGN KEY ("BasketId")
	  REFERENCES "BOOKSTORE"."Baskets" ("Id") ENABLE;
  ALTER TABLE "BOOKSTORE"."BasketItems" ADD CONSTRAINT "FK_BookBasketItem" FOREIGN KEY ("BookId")
	  REFERENCES "BOOKSTORE"."Books" ("Id") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table Baskets
--------------------------------------------------------

  ALTER TABLE "BOOKSTORE"."Baskets" ADD CONSTRAINT "FK_BasketPromo" FOREIGN KEY ("PromoId")
	  REFERENCES "BOOKSTORE"."Promoes" ("Id") ENABLE;
  ALTER TABLE "BOOKSTORE"."Baskets" ADD CONSTRAINT "FK_UserBasket" FOREIGN KEY ("UserId")
	  REFERENCES "BOOKSTORE"."Users" ("Id") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table BookCategory
--------------------------------------------------------

  ALTER TABLE "BOOKSTORE"."BookCategory" ADD CONSTRAINT "FK_BookCategory_Book" FOREIGN KEY ("Books_Id")
	  REFERENCES "BOOKSTORE"."Books" ("Id") ENABLE;
  ALTER TABLE "BOOKSTORE"."BookCategory" ADD CONSTRAINT "FK_BookCategory_Category" FOREIGN KEY ("Categories_Id")
	  REFERENCES "BOOKSTORE"."Categories" ("Id") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table Promoes
--------------------------------------------------------

  ALTER TABLE "BOOKSTORE"."Promoes" ADD CONSTRAINT "FK_AdministratorPromo" FOREIGN KEY ("AdministratorId")
	  REFERENCES "BOOKSTORE"."Administrators" ("Id") ENABLE;
--------------------------------------------------------
--  DDL for Package CONSTANTS
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "BOOKSTORE"."CONSTANTS" AS

-- Oracle DataProvider ODP.NET doesn't work with boolean datatype
c_True CONSTANT NUMBER(1) := 1;
c_False CONSTANT NUMBER(1) := 0;

-- Exceptions
c_Code_NotUnqEmail CONSTANT NUMBER(7) :=  -20001;
c_Message_NotUnqEmail CONSTANT NVARCHAR2(255) :=  'Email is not unique';

c_Code_InvalidBookId CONSTANT NUMBER(7) :=  -20002;
c_Message_InvalidBookId CONSTANT NVARCHAR2(255) :=  'Book with id doesn''t exist';

c_Code_InvalidCategoryId CONSTANT NUMBER(7) :=  -20003;
c_Message_InvalidCategoryId CONSTANT NVARCHAR2(255) :=  'Invalid category id';

c_Code_NotUnqCategoryName CONSTANT NUMBER(7) :=  -20005;
c_Message_NotUnqCategoryName CONSTANT NVARCHAR2(255) :=  'Category with that name already exists';

c_Code_InvalidUserId CONSTANT NUMBER(7) :=  -20011;
c_Message_InvalidUserId CONSTANT NVARCHAR2(255) :=  'User with such Id doesn''t exist';

c_Code_InvalidBasketId CONSTANT NUMBER(7) :=  -20021;
c_Message_InvalidBasketId CONSTANT NVARCHAR2(255) :=  'Basket with ID doesn''t exist';

c_Code_InvalidPromoCode CONSTANT NUMBER(7) :=  -20022;
c_Message_InvalidPromoCode CONSTANT NVARCHAR2(255) :=  'Invalid or Expired Promo Code';
END CONSTANTS;

/
--------------------------------------------------------
--  DDL for Package EXCEPTIONS
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "BOOKSTORE"."EXCEPTIONS" AS
exception_no_userId EXCEPTION; -- No user id exist Definition
exception_no_bookId EXCEPTION; -- No Book id exist custom excpetion
exception_NoBasketExist EXCEPTION;
exception_InvalidPromoCode EXCEPTION;
END EXCEPTIONS;

/
--------------------------------------------------------
--  DDL for Procedure ADMIN_LOGIN
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."ADMIN_LOGIN" 
(
  p_email "Administrators"."Email"%TYPE,
  p_pass "Administrators"."Password"%TYPE,
  p_adminId OUT "Administrators"."Id"%TYPE
)
IS
BEGIN
  select "Id"
  into p_adminId
  from "Administrators" u
  where u."Email" = p_email AND u."Password" = p_pass;

EXCEPTION 
  WHEN NO_DATA_FOUND THEN
    p_adminId := -1;
END;

/
--------------------------------------------------------
--  DDL for Procedure BASKET_ADD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."BASKET_ADD" 
-- creates a basket if not already exists. if book not exists add a book into basket,
-- if book exists already, increase quantity
(
p_userId in "Users"."Id"%TYPE,
p_bookId in "Books"."Id"%TYPE
)
IS
book_count NUMBER := 0;
-- basket_count NUMBER;

CURSOR C1 IS SELECT "Id" FROM "Baskets" WHERE "UserId" = p_userId AND "Baskets"."DateFinished" IS NULL;

userExist NUMBER;
bookExist NUMBER;
basketId "Baskets"."Id"%TYPE;
price "Books"."Price"%TYPE;
BEGIN
CHECK_USERID_EXIST(p_userId, userExist);
IF userExist = CONSTANTS.c_False THEN RAISE EXCEPTIONS.exception_no_userId; END IF;

CHECK_BOOKID_EXIST(p_bookId, bookExist);
IF bookExist = CONSTANTS.c_False THEN RAISE EXCEPTIONS.exception_no_bookId; END IF;

OPEN C1;
FETCH c1 INTO basketId;
IF C1%NOTFOUND THEN
CREATE_BASKET(p_userId, basketId);
END IF;
CLOSE C1;

SELECT COUNT("BookId") INTO book_count FROM "BasketItems" WHERE "BasketId" = basketId AND "BookId" = p_bookId;

IF(book_count = 0) THEN
SELECT "Price" INTO price FROM "Books" WHERE "Id" = p_bookId;
INSERT INTO "BasketItems" VALUES (BOOKSTORE.SEQ_BASKET_ITEM_ID.NEXTVAL, 1, basketId, p_bookId, price);
ELSE
UPDATE "BasketItems" SET "Quantity" = "Quantity" + 1 WHERE "BasketId" = basketId AND "BookId" = p_bookId;
END IF;
commit;
EXCEPTION
WHEN EXCEPTIONS.exception_no_userId THEN
RAISE_APPLICATION_ERROR(CONSTANTS.c_Code_InvalidUserId, CONSTANTS.c_Message_InvalidUserId);

WHEN EXCEPTIONS.exception_no_bookId THEN
RAISE_APPLICATION_ERROR(CONSTANTS.c_Code_InvalidBookId, CONSTANTS.c_Message_InvalidBookId);

END "BASKET_ADD";

/
--------------------------------------------------------
--  DDL for Procedure BASKET_APPLY_PROMO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."BASKET_APPLY_PROMO" 
-- DEPENDENCY PROCEDURE : BASKET_GET_TOTAL
-- return cart calculations while applying promo. Can also do checkout if specified in parameter
(
p_userId IN "Baskets"."UserId"%TYPE,
p_promoCode IN "Promoes"."Code"%TYPE,
doCheckout IN NUMBER, -- 1 : calculate with promo and CHECKOUT basket, 0 : calculate with promo and DON't checkout (don't save)
total_qty OUT NUMBER,
total_price_old OUT "Books"."Price"%TYPE, -- old price without discount
total_discount OUT "Baskets"."DISCOUNT"%TYPE, -- discount in dollars based on total_price_old
total_price_new OUT "Books"."Price"%TYPE,  -- new price with discount
total_tax OUT "Books"."Price"%TYPE, -- tax with discount
total_grand OUT "Books"."Price"%TYPE -- grand total with discount
)
IS
TYPE basket_info_type IS RECORD (basketId "Baskets"."Id"%TYPE);
CURSOR C1 IS SELECT "Id" FROM "Baskets" WHERE "UserId" = p_userId AND "DateFinished" IS NULL;
basket_info basket_info_type;

promoes_rec "Promoes"%ROWTYPE;
promoCode "Promoes"."Code"%TYPE;
CURSOR C2 IS SELECT * INTO promoes_rec FROM "Promoes" WHERE LOWER("Code") = promoCode AND SYSDATE < "Expiry";
BEGIN
promoCode := LOWER(p_promoCode);

OPEN C2;
FETCH C2 INTO promoes_rec;
IF C2%NOTFOUND THEN
RAISE EXCEPTIONS.exception_InvalidPromoCode;
END IF;
CLOSE C2;

BASKET_GET_TOTAL(p_userId, total_qty, total_price_old, total_tax, total_grand);
total_discount := total_price_old * promoes_rec."Discount" / 100;
total_price_new := total_price_old - total_discount;
total_tax := total_price_new * 0.13;
total_grand := total_price_new + total_tax;

--update promoCode and discount in baskets table
IF doCheckout = 1 THEN
UPDATE "Baskets" SET "DateFinished" = SYSDATE, "DISCOUNT" = total_discount WHERE "UserId" = p_userId AND "DateFinished" IS NULL;
END IF;
EXCEPTION
WHEN EXCEPTIONS.exception_InvalidPromoCode THEN
RAISE_APPLICATION_ERROR(CONSTANTS.c_Code_InvalidPromoCode, CONSTANTS.c_Message_InvalidPromoCode);
END BASKET_APPLY_PROMO;

/
--------------------------------------------------------
--  DDL for Procedure BASKET_CHANGE_QTY
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."BASKET_CHANGE_QTY" 
-- change quantity of a product
(
p_userId IN "Users"."Id"%TYPE,
p_bookId IN "BasketItems"."BookId"%TYPE,
in_newQty IN "BasketItems"."Quantity"%TYPE
)
IS
TYPE basket_info_type IS RECORD (basketId "Baskets"."Id"%TYPE);
CURSOR C1 IS SELECT "Id" FROM "Baskets" WHERE "UserId" = p_userId AND "DateFinished" IS NULL;
basket_info basket_info_type;
BEGIN
OPEN C1;
FETCH c1 INTO basket_info;
IF C1%NOTFOUND THEN
RAISE EXCEPTIONS.exception_NoBasketExist;
END IF;
CLOSE C1;

IF in_newQty > 0 THEN
UPDATE "BasketItems" SET "Quantity" = in_newQty WHERE "BasketId" = basket_info.basketId AND "BookId" = p_bookId;
ELSE
BASKET_REMOVE(p_userId, p_bookId);
END IF;
EXCEPTION
WHEN EXCEPTIONS.exception_NoBasketExist THEN
RAISE_APPLICATION_ERROR(CONSTANTS.c_Code_InvalidBasketId, CONSTANTS.c_Message_InvalidBasketId);
END BASKET_CHANGE_QTY;

/
--------------------------------------------------------
--  DDL for Procedure BASKET_CHECKOUT
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."BASKET_CHECKOUT" 
-- dependency procedure : BASKET_REMOVE
-- check out and places final order
(
p_userId IN "Users"."Id"%TYPE
)
IS
basketExist NUMBER;
BEGIN
SELECT COUNT("Id") INTO basketExist FROM "Baskets" WHERE "UserId" = p_userId;

IF basketExist = CONSTANTS.c_False THEN
RAISE EXCEPTIONS.exception_NoBasketExist;
ELSE
UPDATE "Baskets" SET "DateFinished" = SYSDATE WHERE "UserId" = p_userId AND "DateFinished" IS NULL;
END IF;
EXCEPTION
WHEN EXCEPTIONS.exception_NoBasketExist THEN
RAISE_APPLICATION_ERROR(CONSTANTS.c_Code_InvalidBasketId, CONSTANTS.c_Message_InvalidBasketId);
END BASKET_CHECKOUT;

/
--------------------------------------------------------
--  DDL for Procedure BASKET_GET_BY_USER
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."BASKET_GET_BY_USER" 
-- returns a user's active cart
(
p_userId IN "Baskets"."UserId"%TYPE,
p_result OUT SYS_REFCURSOR
)
IS
TYPE basket_info_type IS RECORD (basketId "Baskets"."Id"%TYPE);
CURSOR C1 IS SELECT "Id" FROM "Baskets" WHERE "UserId" = p_userId AND "DateFinished" IS NULL;
basket_info basket_info_type;
BEGIN
OPEN C1;
FETCH c1 INTO basket_info;
IF C1%NOTFOUND THEN
RAISE EXCEPTIONS.exception_NoBasketExist;
END IF;
CLOSE C1;

OPEN p_result FOR SELECT "BasketItems"."Id", "BasketItems"."BookId", "Books"."Title", "BasketItems"."PRICE", "BasketItems"."Quantity", "BasketItems"."Quantity" * "BasketItems"."PRICE" AS subTotal FROM "BasketItems" LEFT JOIN "Books" ON "BasketItems"."BookId" = "Books"."Id" WHERE "BasketId" = basket_info.basketId ;
EXCEPTION
WHEN EXCEPTIONS.exception_NoBasketExist THEN
RAISE_APPLICATION_ERROR(CONSTANTS.c_Code_InvalidBasketId, CONSTANTS.c_Message_InvalidBasketId);
END BASKET_GET_BY_USER;

/
--------------------------------------------------------
--  DDL for Procedure BASKET_GET_TOTAL
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."BASKET_GET_TOTAL" 
-- returns cart calculations
(
p_userId IN "Baskets"."UserId"%TYPE,
total_qty OUT NUMBER,
total_price OUT "Books"."Price"%TYPE,
total_tax OUT "Books"."Price"%TYPE,
total_grand OUT "Books"."Price"%TYPE
)
IS
TYPE basket_info_type IS RECORD (basketId "Baskets"."Id"%TYPE);
CURSOR C1 IS SELECT "Id" FROM "Baskets" WHERE "UserId" = p_userId AND "DateFinished" IS NULL;
basket_info basket_info_type;
BEGIN
OPEN C1;
FETCH c1 INTO basket_info;
IF C1%NOTFOUND THEN
RAISE EXCEPTIONS.exception_NoBasketExist;
END IF;
CLOSE C1;

SELECT SUM("Quantity"), SUM("Quantity" * "PRICE"), (SUM("Quantity" * "PRICE")* 0.13), (SUM("Quantity" * "PRICE")* 0.13 + SUM("Quantity" * "PRICE")) INTO total_qty, total_price, total_tax, total_grand FROM "BasketItems" WHERE "BasketId" = basket_info.basketId;
EXCEPTION
WHEN EXCEPTIONS.exception_NoBasketExist THEN
RAISE_APPLICATION_ERROR(CONSTANTS.c_Code_InvalidBasketId, CONSTANTS.c_Message_InvalidBasketId);
END BASKET_GET_TOTAL;

/
--------------------------------------------------------
--  DDL for Procedure BASKET_REMOVE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."BASKET_REMOVE" 
-- remove a book from cart
(
p_userId IN "Users"."Id"%TYPE,
p_bookID IN "BasketItems"."BookId"%TYPE
)
IS
TYPE basket_info_type IS RECORD (basketId "Baskets"."Id"%TYPE);
CURSOR C1 IS SELECT "Id" FROM "Baskets" WHERE "UserId" = p_userId AND "DateFinished" IS NULL;
basket_info basket_info_type;
BEGIN
OPEN C1;
FETCH c1 INTO basket_info;
IF C1%NOTFOUND THEN
RAISE EXCEPTIONS.exception_NoBasketExist;
END IF;
CLOSE C1;

DELETE FROM "BasketItems" WHERE "BasketId" = basket_info.basketId AND "BookId" = p_bookID;
EXCEPTION
WHEN EXCEPTIONS.exception_NoBasketExist THEN
RAISE_APPLICATION_ERROR(CONSTANTS.c_Code_InvalidBasketId, CONSTANTS.c_Message_InvalidBasketId);
END BASKET_REMOVE;

/
--------------------------------------------------------
--  DDL for Procedure BOOKS_ADD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."BOOKS_ADD" 
( 
  p_title "Books"."Title"%TYPE,
  p_isbn "Books"."ISBN"%TYPE,  
  p_author "Books"."Author"%TYPE,
  p_publisher "Books"."Publisher"%TYPE,
  p_year "Books"."Year"%TYPE,
  p_price "Books"."Price"%TYPE,
  p_searchCounts "Books"."SearchCounts"%TYPE
)
IS
BEGIN
  Insert into BOOKSTORE."Books" ("Id","Title",ISBN,"Author","Publisher","Year","Price","SearchCounts") 
  values (SEQ_BOOKS_ID.NEXTVAL, p_title, p_isbn, p_author, p_publisher, p_year, p_price, p_searchCounts);
END;

/
--------------------------------------------------------
--  DDL for Procedure BOOKS_GET
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."BOOKS_GET" 
(p_result OUT SYS_REFCURSOR)
IS
BEGIN
  OPEN p_result FOR select * from "Books" order by "Books"."Id";
END;

/
--------------------------------------------------------
--  DDL for Procedure BOOKS_GET_BY_ID
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."BOOKS_GET_BY_ID" 
(
p_id "Books"."Id"%TYPE, 
p_result OUT SYS_REFCURSOR)
IS

BEGIN
  
  OPEN p_result FOR select * from "Books" where "Id" = p_id;
  
END;

/
--------------------------------------------------------
--  DDL for Procedure BOOKS_GETSHORTDESCRIPTION
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."BOOKS_GETSHORTDESCRIPTION" 
(
p_categoryId "Categories"."Id"%TYPE, 
p_result OUT SYS_REFCURSOR)
IS

BEGIN

  IF p_categoryId <= 0 THEN
  -- get all books
      OPEN p_result FOR select "Id", "Title", "ISBN"  from "Books" b;
  ELSE
  -- get books for specific category
      OPEN p_result FOR select "Id", "Title", "ISBN"  
        from "Books"
        where "Books"."Id" in (select "Books_Id" from "BookCategory" where "Categories_Id" = p_categoryId);
  END IF;
  
END;

/
--------------------------------------------------------
--  DDL for Procedure BOOKS_IS_ISBN_UNIQUE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."BOOKS_IS_ISBN_UNIQUE" 
(
  p_isbn "Books"."ISBN"%TYPE,
  p_exceptForId "Books"."Id"%TYPE := -1,
  p_isUnique OUT NUMBER
)
AS 
  lv_rowsCount NUMBER(1);
BEGIN
  select count(*)
  into lv_rowsCount
  from "Books"
  where "Books"."ISBN" = p_isbn AND "Id" != p_exceptForId;
  
  if lv_rowsCount > 0 then 
    p_isUnique := CONSTANTS.c_False;
  else
      p_isUnique := CONSTANTS.c_True;
  end if;
END BOOKS_IS_ISBN_UNIQUE;

/
--------------------------------------------------------
--  DDL for Procedure BOOKS_UPDATE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."BOOKS_UPDATE" 
( 
  p_id "Books"."Id"%TYPE,
  p_title "Books"."Title"%TYPE,
  p_isbn "Books"."ISBN"%TYPE,  
  p_author "Books"."Author"%TYPE,
  p_publisher "Books"."Publisher"%TYPE,
  p_year "Books"."Year"%TYPE,
  p_price "Books"."Price"%TYPE,
  p_searchCounts "Books"."SearchCounts"%TYPE
)
IS
BEGIN

  update "Books"
  set "Title" = p_title,
  "ISBN" = p_isbn,
  "Author" = p_author,
  "Publisher" = p_publisher,
  "Year" = p_year,
  "Price"= p_price,
  "SearchCounts" = p_searchCounts
  where "Id" = p_id;
  
  IF SQL%ROWCOUNT = 0 THEN
      raise_application_error(-20004, 'Invalid book id');
  END IF;

END;

/
--------------------------------------------------------
--  DDL for Procedure CATEGORIES_ADD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."CATEGORIES_ADD" 
(
  p_name "Categories"."Name"%TYPE,
  p_id OUT "Categories"."Id"%TYPE
)
IS
  lv_isCategoryUnique NUMBER(10);
BEGIN
  CATEGORIES_IS_NAME_UNIQUE(p_name, -1, lv_isCategoryUnique);
  
  IF lv_isCategoryUnique = CONSTANTS.c_False THEN
    raise_application_error(CONSTANTS.c_Code_NotUnqCategoryName, CONSTANTS.c_Message_NotUnqCategoryName);
  END IF;
  
  p_id := SEQ_CATEGORY_ID.NEXTVAL;
  insert into "Categories" values(p_id, p_name);
END;

/
--------------------------------------------------------
--  DDL for Procedure CATEGORIES_BOOK_ADD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."CATEGORIES_BOOK_ADD" 
(
p_bookId "Books"."Id"%TYPE,
p_categoryId "Categories"."Id"%TYPE
)
IS
  lv_exists NUMBER(1);
BEGIN
  select count(*)
  into lv_exists
  from "BookCategory"
  where "Books_Id" = p_bookId AND "Categories_Id" = p_categoryId;
  
  IF lv_exists > 0 THEN
    RETURN;
  END IF;
  
  insert into "BookCategory" values (p_bookId, p_categoryId);
  
END;

/
--------------------------------------------------------
--  DDL for Procedure CATEGORIES_BOOK_CLEAR
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."CATEGORIES_BOOK_CLEAR" 
(
p_bookId "Books"."Id"%TYPE
)
IS
BEGIN
  
  delete from "BookCategory"
  where "Books_Id" = p_bookId;

END;

/
--------------------------------------------------------
--  DDL for Procedure CATEGORIES_DELETE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."CATEGORIES_DELETE" 
(
  p_id "Categories"."Id"%TYPE
)
IS

BEGIN
  DELETE from "BookCategory" where "Categories_Id" = p_id;
  DELETE from "Categories" where "Id" = p_id;
  
  if SQL%NOTFOUND THEN
    raise_application_error(CONSTANTS.c_Code_InvalidCategoryId, CONSTANTS.c_Message_InvalidCategoryId);
  end if;
END;

/
--------------------------------------------------------
--  DDL for Procedure CATEGORIES_GET
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."CATEGORIES_GET" 
(p_result OUT SYS_REFCURSOR)
IS

BEGIN
  OPEN p_result FOR select *
        from "Categories" c
        order by c."Name";
END;

/
--------------------------------------------------------
--  DDL for Procedure CATEGORIES_GET_FOR_BOOK
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."CATEGORIES_GET_FOR_BOOK" 
(
  p_bookId "Books"."Id"%TYPE,
  result OUT SYS_REFCURSOR
)
IS
BEGIN
  OPEN result FOR select c."Id", c."Name" 
    from "BookCategory" bc
    inner join "Categories" c
    on c."Id" = bc."Categories_Id"
    where bc."Books_Id" = p_bookId
    order by c."Name";
END;

/
--------------------------------------------------------
--  DDL for Procedure CATEGORIES_GET_NAME_BY_ID
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."CATEGORIES_GET_NAME_BY_ID" 
(
  p_id "Categories"."Id"%TYPE,
  p_name OUT "Categories"."Name"%TYPE
)
IS
BEGIN
  select c."Name"
  into p_name
  from "Categories" c
  where c."Id" = p_id;
  
EXCEPTION
  when NO_DATA_FOUND then
    raise_application_error(-20003, 'Invalid category id');
END;

/
--------------------------------------------------------
--  DDL for Procedure CATEGORIES_GET_WITHBOOKS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."CATEGORIES_GET_WITHBOOKS" 
(p_result OUT SYS_REFCURSOR)
IS

BEGIN
  OPEN p_result FOR select *
        from "Categories" c
        where c."Id" in (select distinct "Categories_Id" from "BookCategory")
        order by c."Name";
END;

/
--------------------------------------------------------
--  DDL for Procedure CATEGORIES_IS_NAME_UNIQUE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."CATEGORIES_IS_NAME_UNIQUE" 
(
  p_name "Categories"."Name"%TYPE,
  p_exceptForId "Categories"."Id"%TYPE := -1,
  p_isUnique OUT NUMBER
)
AS 
  lv_rowsCount NUMBER(10);
BEGIN
  select count(*)
  into lv_rowsCount
  from "Categories"
  where "Name" = p_name and "Id" != p_exceptForId;
  
  if lv_rowsCount > 0 then 
    p_isUnique := CONSTANTS.c_False;
  else
      p_isUnique := CONSTANTS.c_True;
  end if;
END;

/
--------------------------------------------------------
--  DDL for Procedure CATEGORIES_SET_FOR_BOOK
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."CATEGORIES_SET_FOR_BOOK" 
(
  p_bookId "Books"."Id"%TYPE,
  p_categoryId "Categories"."Id"%TYPE
)
IS
  lv_exists number(10);
BEGIN
  select count(*)
  into lv_exists
  from "BookCategory";
  
  IF lv_exists > 0 THEN
    RETURN;
  END IF;

  Insert into BOOKSTORE."BookCategory" ("Books_Id","Categories_Id") 
  values (p_bookId, p_categoryId);
END;

/
--------------------------------------------------------
--  DDL for Procedure CATEGORIES_UPDATE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."CATEGORIES_UPDATE" 
(
  p_id "Categories"."Id"%TYPE,
  p_name "Categories"."Name"%TYPE
)
IS
  lv_isCategoryUnique NUMBER(10);
BEGIN

  CATEGORIES_IS_NAME_UNIQUE(p_name, p_id, lv_isCategoryUnique);
  IF lv_isCategoryUnique = CONSTANTS.c_False THEN
    raise_application_error(CONSTANTS.c_Code_NotUnqCategoryName, CONSTANTS.c_Message_NotUnqCategoryName);
  END IF;

  update "Categories" SET "Name" = p_name where "Id" = p_id;
  
  if sql%NOTFOUND then
    raise_application_error(CONSTANTS.c_Code_InvalidCategoryId, CONSTANTS.c_Message_InvalidCategoryId);
  end if;
END;

/
--------------------------------------------------------
--  DDL for Procedure CHECK_BOOKID_EXIST
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."CHECK_BOOKID_EXIST" 
-- check if a book with particular id exist in database
(
bookId in "Books"."Id"%TYPE,
bookExists OUT NUMBER
)
IS
book_count NUMBER := 0;
BEGIN
SELECT count("Id") INTO book_count FROM "Books" WHERE "Id" = bookId;
IF book_count <= 0 THEN
bookExists := CONSTANTS.c_False;
ELSE
bookExists := CONSTANTS.c_True;
END IF;
END "CHECK_BOOKID_EXIST";

/
--------------------------------------------------------
--  DDL for Procedure CHECK_UNQIUE_EMAIL
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."CHECK_UNQIUE_EMAIL" 
(
  p_email "Users"."Email"%TYPE,
  p_isUnique OUT NUMBER --cannot use boolean because odp.net provider doesn't support it properly
)
IS
  lv_count NUMBER(20);
BEGIN
  --check user table
  select count(*)
  into lv_count
  from "Users" u
  where u."Email" = p_email;
  
  IF lv_count > 0 THEN
    p_isUnique := 0;
    return;
  END IF;
  
  --check admin table
  select count(*)
  into lv_count
  from "Administrators" a
  where a."Email" = p_email;
  
  IF lv_count > 0 THEN
    p_isUnique := 0;
    return;
  END IF;
  
  p_isUnique := 1;
END;

/
--------------------------------------------------------
--  DDL for Procedure CHECK_USERID_EXIST
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."CHECK_USERID_EXIST" 
-- checks whether a particular user id exist in database
(
userId in "Users"."Id"%TYPE,
userExists OUT NUMBER
)
IS
user_count NUMBER := 0;
BEGIN
SELECT count("Id") INTO user_count FROM "Users" WHERE "Id" = userId;
IF user_count <= 0 THEN
userExists := CONSTANTS.c_False;
ELSE
userExists := CONSTANTS.c_True;
END IF;
END "CHECK_USERID_EXIST";

/
--------------------------------------------------------
--  DDL for Procedure CREATE_BASKET
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."CREATE_BASKET" 
-- create a basket. adds user id into basket table
(
userId IN "Users"."Id"%TYPE,
basketId OUT "Baskets"."Id"%TYPE
)
IS
BEGIN
basketId := SEQ_BASKET_ID.NEXTVAL;
INSERT INTO "Baskets" VALUES (basketId, NULL, NULL, userId, NULL);
END CREATE_BASKET;

/
--------------------------------------------------------
--  DDL for Procedure PROMOES_GET_DETAILS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."PROMOES_GET_DETAILS" 
(p_result OUT SYS_REFCURSOR)
AS 
BEGIN
  OPEN p_result for select p."Id", p."Code", p."Expiry", p."AdministratorId", a."Email", p."Discount"
              from "Promoes" p
              inner join "Administrators" a
              on p."AdministratorId" = a."Id";
END ;

/
--------------------------------------------------------
--  DDL for Procedure TEST
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."TEST" 
(
p_userId in "Users"."Id"%TYPE
)
IS
basketId "Baskets"."Id"%TYPE;
BEGIN
SELECT "Id" INTO basketId FROM "Baskets" WHERE "UserId" = p_userId AND "Baskets"."DateFinished" IS NULL;

IF SQL%NOTFOUND THEN
DBMS_OUTPUT.PUT_LINE('will create a new one don''t worry');
END IF;
END;

/
--------------------------------------------------------
--  DDL for Procedure USERS_GET
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."USERS_GET" 
(p_result OUT SYS_REFCURSOR)
IS
BEGIN
  OPEN p_result FOR select * from "Users" order by "Users"."Id";
END;

/
--------------------------------------------------------
--  DDL for Procedure USERS_LOGIN
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."USERS_LOGIN" 
(
  p_email "Users"."Email"%TYPE,
  p_pass "Users"."Password"%TYPE,
  p_userId OUT "Users"."Id"%TYPE
)
IS
BEGIN
  select "Id"
  into p_userId
  from "Users" u
  where u."Email" = p_email AND u."Password" = p_pass;

EXCEPTION 
  WHEN NO_DATA_FOUND THEN
  p_userId := -1;
END;

/
--------------------------------------------------------
--  DDL for Procedure USERS_REGISTER
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BOOKSTORE"."USERS_REGISTER" 
(
  p_email "Users"."Email"%TYPE,
  p_password "Users"."Password"%TYPE,
  p_firstName "Users"."FirstName"%TYPE,
  p_lastName "Users"."LastName"%TYPE,
  p_birthday "Users"."Birhday"%TYPE,
  p_dateOfRegistration "Users"."DateOfRegistration"%TYPE,
  p_phone "Users"."PhoneNumber"%TYPE,
  p_address "Users"."Address"%TYPE,
  p_postalCode "Users"."PostalCode"%TYPE,
  p_city "Users"."City"%TYPE,
  p_state "Users"."State"%TYPE
)
IS
  lv_isEmailUnique NUMBER(1);
BEGIN
  -- validation
  CHECK_UNQIUE_EMAIL(p_email, lv_isEmailUnique);
  
  IF lv_isEmailUnique = 0 THEN
    raise_application_error(-20001, 'Email is not unique');
    RETURN;
  END IF;
  
  -- register
  Insert into BOOKSTORE."Users" ("Id","Email","Password","FirstName","LastName","Birhday","DateOfRegistration","PhoneNumber","Address","PostalCode","City","State") 
  values (SEQ_USERS_ID.NEXTVAL, p_email, p_password, p_firstName, p_lastName, p_birthday, p_dateOfRegistration, p_phone, p_address, p_postalCode, p_city, p_state);
  
END;

/
