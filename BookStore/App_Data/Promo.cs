//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BookStore.App_Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Promo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Promo()
        {
            this.Baskets = new HashSet<Basket>();
        }
    
        public int Id { get; set; }
        public string Code { get; set; }
        public System.DateTime Expiry { get; set; }
        public int AdministratorId { get; set; }
        public decimal Discount { get; set; }
    
        public virtual Administrator Administrator { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Basket> Baskets { get; set; }
    }
}
