-- --------------------------------------------------
-- Entity Designer DDL Script for Oracle database
-- --------------------------------------------------
-- Date Created: 2016-04-15 8:03:17 PM
-- Generated from EDMX file: C:\Users\andry_ad\Documents\!COLLEGE\Adv. Web Development\Assignments\BookStore\BookStore\BookStore\App_Data\Database.edmx
-- --------------------------------------------------

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

-- ALTER TABLE "BOOKSTORE"."Promoes" DROP CONSTRAINT "FK_AdministratorPromo" CASCADE;

-- ALTER TABLE "BOOKSTORE"."BasketItems" DROP CONSTRAINT "FK_BasketBasketItem" CASCADE;

-- ALTER TABLE "BOOKSTORE"."Baskets" DROP CONSTRAINT "FK_BasketPromo" CASCADE;

-- ALTER TABLE "BOOKSTORE"."BasketItems" DROP CONSTRAINT "FK_BookBasketItem" CASCADE;

-- ALTER TABLE "BOOKSTORE"."BookCategory" DROP CONSTRAINT "FK_BookCategory_Book" CASCADE;

-- ALTER TABLE "BOOKSTORE"."BookCategory" DROP CONSTRAINT "FK_BookCategory_Category" CASCADE;

-- ALTER TABLE "BOOKSTORE"."Baskets" DROP CONSTRAINT "FK_UserBasket" CASCADE;
-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

-- DROP TABLE "BOOKSTORE"."Administrators";

-- DROP TABLE "BOOKSTORE"."BasketItems";

-- DROP TABLE "BOOKSTORE"."Baskets";

-- DROP TABLE "BOOKSTORE"."BookCategory";

-- DROP TABLE "BOOKSTORE"."Books";

-- DROP TABLE "BOOKSTORE"."Categories";

-- DROP TABLE "BOOKSTORE"."Logs";

-- DROP TABLE "BOOKSTORE"."Promoes";

-- DROP TABLE "BOOKSTORE"."Users";

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Administrators'
CREATE TABLE "BOOKSTORE"."Administrators" (
   "Id" NUMBER(10) NOT NULL,
   "Email" NVARCHAR2(100) NOT NULL,
   "Password" NVARCHAR2(50) NOT NULL
);

-- Creating table 'BasketItems'
CREATE TABLE "BOOKSTORE"."BasketItems" (
   "Id" NUMBER(10) NOT NULL,
   "Quantity" NUMBER(10) NOT NULL,
   "BasketId" NUMBER(10) NOT NULL,
   "BookId" NUMBER(10) NOT NULL
);

-- Creating table 'Baskets'
CREATE TABLE "BOOKSTORE"."Baskets" (
   "Id" NUMBER(10) NOT NULL,
   "DateFinished" DATE ,
   "PromoId" NUMBER(10) ,
   "UserId" NUMBER(10) 
);

-- Creating table 'Books'
CREATE TABLE "BOOKSTORE"."Books" (
   "Id" NUMBER(10) NOT NULL,
   "Title" NVARCHAR2(255) NOT NULL,
   "ISBN" NVARCHAR2(20) NOT NULL,
   "Author" NVARCHAR2(100) NOT NULL,
   "Publisher" NVARCHAR2(100) NOT NULL,
   "Year" DATE NOT NULL,
   "Price" NUMBER(36,2) NOT NULL,
   "SearchCounts" NUMBER(10) NOT NULL
);

-- Creating table 'Categories'
CREATE TABLE "BOOKSTORE"."Categories" (
   "Id" NUMBER(10) NOT NULL,
   "Name" NVARCHAR2(100) NOT NULL
);

-- Creating table 'Logs'
CREATE TABLE "BOOKSTORE"."Logs" (
   "Id" NUMBER(10) NOT NULL,
   "Date" DATE NOT NULL,
   "Title" NVARCHAR2(100) NOT NULL,
   "Description" NVARCHAR2(255) NOT NULL,
   "User" NUMBER(10) NOT NULL
);

-- Creating table 'Promoes'
CREATE TABLE "BOOKSTORE"."Promoes" (
   "Id" NUMBER(10) NOT NULL,
   "Code" NVARCHAR2(20) NOT NULL,
   "Expiry" DATE NOT NULL,
   "AdministratorId" NUMBER(10) NOT NULL,
   "Discount" NUMBER(38) NOT NULL
);

-- Creating table 'Users'
CREATE TABLE "BOOKSTORE"."Users" (
   "Id" NUMBER(10) NOT NULL,
   "Email" NVARCHAR2(100) NOT NULL,
   "Password" NVARCHAR2(50) NOT NULL,
   "FirstName" NVARCHAR2(100) NOT NULL,
   "LastName" NVARCHAR2(100) NOT NULL,
   "Birhday" DATE NOT NULL,
   "DateOfRegistration" DATE NOT NULL,
   "PhoneNumber" NVARCHAR2(100) NOT NULL,
   "Address" NVARCHAR2(250) NOT NULL,
   "PostalCode" NVARCHAR2(20) NOT NULL,
   "City" NVARCHAR2(100) NOT NULL,
   "State" NVARCHAR2(50) NOT NULL
);

-- Creating table 'BookCategory'
CREATE TABLE "BOOKSTORE"."BookCategory" (
   "Books_Id" NUMBER(10) NOT NULL,
   "Categories_Id" NUMBER(10) NOT NULL
);


-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on "Id"in table 'Administrators'
ALTER TABLE "BOOKSTORE"."Administrators"
ADD CONSTRAINT "PK_Administrators"
   PRIMARY KEY ("Id" )
   ENABLE
   VALIDATE;


-- Creating primary key on "Id"in table 'BasketItems'
ALTER TABLE "BOOKSTORE"."BasketItems"
ADD CONSTRAINT "PK_BasketItems"
   PRIMARY KEY ("Id" )
   ENABLE
   VALIDATE;


-- Creating primary key on "Id"in table 'Baskets'
ALTER TABLE "BOOKSTORE"."Baskets"
ADD CONSTRAINT "PK_Baskets"
   PRIMARY KEY ("Id" )
   ENABLE
   VALIDATE;


-- Creating primary key on "Id"in table 'Books'
ALTER TABLE "BOOKSTORE"."Books"
ADD CONSTRAINT "PK_Books"
   PRIMARY KEY ("Id" )
   ENABLE
   VALIDATE;


-- Creating primary key on "Id"in table 'Categories'
ALTER TABLE "BOOKSTORE"."Categories"
ADD CONSTRAINT "PK_Categories"
   PRIMARY KEY ("Id" )
   ENABLE
   VALIDATE;


-- Creating primary key on "Id"in table 'Logs'
ALTER TABLE "BOOKSTORE"."Logs"
ADD CONSTRAINT "PK_Logs"
   PRIMARY KEY ("Id" )
   ENABLE
   VALIDATE;


-- Creating primary key on "Id"in table 'Promoes'
ALTER TABLE "BOOKSTORE"."Promoes"
ADD CONSTRAINT "PK_Promoes"
   PRIMARY KEY ("Id" )
   ENABLE
   VALIDATE;


-- Creating primary key on "Id"in table 'Users'
ALTER TABLE "BOOKSTORE"."Users"
ADD CONSTRAINT "PK_Users"
   PRIMARY KEY ("Id" )
   ENABLE
   VALIDATE;


-- Creating primary key on "Books_Id", "Categories_Id"in table 'BookCategory'
ALTER TABLE "BOOKSTORE"."BookCategory"
ADD CONSTRAINT "PK_BookCategory"
   PRIMARY KEY ("Books_Id", "Categories_Id" )
   ENABLE
   VALIDATE;


-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on "AdministratorId" in table 'Promoes'
ALTER TABLE "BOOKSTORE"."Promoes"
ADD CONSTRAINT "FK_AdministratorPromo"
   FOREIGN KEY ("AdministratorId")
   REFERENCES "BOOKSTORE"."Administrators"
       ("Id")
   ENABLE
   VALIDATE;

-- Creating index for FOREIGN KEY 'FK_AdministratorPromo'
CREATE INDEX "IX_FK_AdministratorPromo"
ON "BOOKSTORE"."Promoes"
   ("AdministratorId");

-- Creating foreign key on "BasketId" in table 'BasketItems'
ALTER TABLE "BOOKSTORE"."BasketItems"
ADD CONSTRAINT "FK_BasketBasketItem"
   FOREIGN KEY ("BasketId")
   REFERENCES "BOOKSTORE"."Baskets"
       ("Id")
   ENABLE
   VALIDATE;

-- Creating index for FOREIGN KEY 'FK_BasketBasketItem'
CREATE INDEX "IX_FK_BasketBasketItem"
ON "BOOKSTORE"."BasketItems"
   ("BasketId");

-- Creating foreign key on "BookId" in table 'BasketItems'
ALTER TABLE "BOOKSTORE"."BasketItems"
ADD CONSTRAINT "FK_BookBasketItem"
   FOREIGN KEY ("BookId")
   REFERENCES "BOOKSTORE"."Books"
       ("Id")
   ENABLE
   VALIDATE;

-- Creating index for FOREIGN KEY 'FK_BookBasketItem'
CREATE INDEX "IX_FK_BookBasketItem"
ON "BOOKSTORE"."BasketItems"
   ("BookId");

-- Creating foreign key on "PromoId" in table 'Baskets'
ALTER TABLE "BOOKSTORE"."Baskets"
ADD CONSTRAINT "FK_BasketPromo"
   FOREIGN KEY ("PromoId")
   REFERENCES "BOOKSTORE"."Promoes"
       ("Id")
   ENABLE
   VALIDATE;

-- Creating index for FOREIGN KEY 'FK_BasketPromo'
CREATE INDEX "IX_FK_BasketPromo"
ON "BOOKSTORE"."Baskets"
   ("PromoId");

-- Creating foreign key on "UserId" in table 'Baskets'
ALTER TABLE "BOOKSTORE"."Baskets"
ADD CONSTRAINT "FK_UserBasket"
   FOREIGN KEY ("UserId")
   REFERENCES "BOOKSTORE"."Users"
       ("Id")
   ENABLE
   VALIDATE;

-- Creating index for FOREIGN KEY 'FK_UserBasket'
CREATE INDEX "IX_FK_UserBasket"
ON "BOOKSTORE"."Baskets"
   ("UserId");

-- Creating foreign key on "Books_Id" in table 'BookCategory'
ALTER TABLE "BOOKSTORE"."BookCategory"
ADD CONSTRAINT "FK_BookCategory_Book"
   FOREIGN KEY ("Books_Id")
   REFERENCES "BOOKSTORE"."Books"
       ("Id")
   ENABLE
   VALIDATE;

-- Creating index for FOREIGN KEY 'FK_BookCategory_Book'
CREATE INDEX "IX_FK_BookCategory_Book"
ON "BOOKSTORE"."BookCategory"
   ("Books_Id");

-- Creating foreign key on "Categories_Id" in table 'BookCategory'
ALTER TABLE "BOOKSTORE"."BookCategory"
ADD CONSTRAINT "FK_BookCategory_Category"
   FOREIGN KEY ("Categories_Id")
   REFERENCES "BOOKSTORE"."Categories"
       ("Id")
   ENABLE
   VALIDATE;

-- Creating index for FOREIGN KEY 'FK_BookCategory_Category'
CREATE INDEX "IX_FK_BookCategory_Category"
ON "BOOKSTORE"."BookCategory"
   ("Categories_Id");

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------
