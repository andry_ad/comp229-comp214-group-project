﻿using System.Text;
using System.Web.UI;

namespace BookStore
{
	public class Helper
	{
		public static void ShowPopUpMsg(Page page, string msg, string redirect = null)
		{
			var sb = new StringBuilder();
			sb.Append("alert('");
			sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("'", "\\'"));
			sb.Append("');");

			if (redirect != null)
			{
				sb.Append("window.location.href = '");
				sb.Append(redirect);
				sb.Append("';");
			}

			ScriptManager.RegisterStartupScript(page, page.GetType(), "showalert", sb.ToString(), true);
		} 
	}
}