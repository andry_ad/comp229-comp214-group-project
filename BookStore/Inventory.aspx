﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Inventory.aspx.cs" Inherits="BookStore.Inventory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">

    <asp:Button runat="server" Text="Add" CssClass="register" ID="bAdd" OnClick="bAdd_OnClick"/>

    <asp:Repeater ID="rBooks" runat="server" OnItemCommand="rBooks_OnItemCommand">

        <HeaderTemplate>
            <table border="0" width="100%">
                <tr>
                    <th>Id</th>
                    <th>Image</th>
                    <th>Title</th>
                    <th>Author</th>
                    <th>Publisher</th>
                    <th>Year</th>
                    <th>Price</th>
                    <th>Edit</th>
                </tr>
        </HeaderTemplate>

        <ItemTemplate>
            <tr>
                <td><%# Eval("Id") %></td>
                <td><img src="productImages/<%# Eval("ISBN") %>.jpg" alt="<%# Eval("ISBN") %>" class="miniThumb"/></td>
                <td><%# Eval("Title") %></td>
                <td><%# Eval("Author") %></td>
                <td><%# Eval("Publisher") %></td>
                <td><%# ((DateTime)Eval("Year")).Year %></td>
                <td><%# Eval("Price") %></td>
                <td><asp:Button runat="server" Text="Edit" CommandName="Edit" CommandArgument='<%# Eval("Id") %>' CssClass="register" UseSubmitBehavior="False"/></td>
            </tr>
        </ItemTemplate>

        <FooterTemplate>
            </table>
        </FooterTemplate>

    </asp:Repeater>

</asp:Content>
