﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BookStore.App_Data;

namespace BookStore
{
	public partial class MasterPage : System.Web.UI.MasterPage
	{
		protected List<CATEGORIES_GET_WITHBOOKS_Result> BooksCategories { get; set; }

		protected int TotalQty { get; private set; }

		internal int CartQuantity { get; private set; }

		public MasterPage()
		{
			BooksCategories = new List<CATEGORIES_GET_WITHBOOKS_Result>();
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			//check if user info was changed
			if (MySession.IsUserLoged && !MySession.ValidateUser())
				Response.Redirect("Login.aspx");

			using (var c = new DatabaseContainer())
			{
				var cat = c.CATEGORIES_GET_WITHBOOKS();

				BooksCategories = c.CATEGORIES_GET_WITHBOOKS().ToList();

				if (MySession.IsUserLoged)
				{
					var cart = Cart.BasketGetTotal(c, MySession.UserOrAdminId);

					lTotalItems.Text = cart.TotalQty.ToString();
					lTotalPrice.Text = cart.TotalPrice.ToString();
				}
			}
		}

		protected void bLogout_OnClick(object sender, EventArgs e)
		{
			MySession.Logout();
			Response.Redirect("Index.aspx");
		}
	}
}