﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="BookStore.Index" %>
<%@ Import Namespace="BookStore" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
	<div class="crumb_nav">
		<a href="index.aspx">home</a> &gt;&gt; <%= CategoryName %>
           
	</div>
	<div class="title"><span class="title_icon">
		<img src="images/bullet1.gif" alt="" title="" /></span>Category books</div>

	<div class="new_products">
        
        <% foreach (var b in Books) { %>
        
            <div class="new_prod_box">
                <a href="<%= Details.GetUrlForBookId((int)b.Id) %>"><%= AdjustTitleLength(b.Title)%></a>
                <div class="new_prod_bg">
                    <a href="<%= Details.GetUrlForBookId((int)b.Id) %>">
                        <img src="productImages/<%= b.ISBN %>.jpg" alt="<%= b.Title%>" class="thumb" border="0" /></a>
                </div>
            </div>
                
        <% } %>

        <!--
		<div class="pagination">
			<span class="disabled"><<</span><span class="current">1</span><a href="#?page=2">2</a><a href="#?page=3">3</a>…<a href="#?page=199">10</a><a href="#?page=200">11</a><a href="#?page=2">>></a>
		</div>
                    -->

	</div>

	<div class="clear"></div>
</asp:Content>
