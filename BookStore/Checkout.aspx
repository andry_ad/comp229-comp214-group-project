﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Checkout.aspx.cs" Inherits="BookStore.Checkout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
	<div class="title">
		<span class="title_icon">
			<img src="images/bullet1.gif" alt="" title="" /></span>Checkout
	</div>

	<div class="feat_prod_box_details">

		<table class="cart_table">
			<tr class="cart_title">
				<td>Item pic</td>
				<td>Book name</td>
				<td>Unit price</td>
				<td>Qty</td>
				<td>Total</td>
			</tr>
			<asp:Repeater ID="rBooks" runat="server">
				<ItemTemplate>
					<tr>
						<td>
							<img src="productImages/<%# Eval("ISBN") %>.jpg" alt="<%# Eval("ISBN") %>" class="cart_thumb" /></td>
						<td style="text-align: left"><%# Eval("Title") %></td>
						<td><%# Eval("PRICE") %>$</td>
						<td><%# Eval("Quantity") %></td>
						<td><%# Eval("SUBTOTAL") %>$</td>
					</tr>

				</ItemTemplate>
			</asp:Repeater>

			<tr>
				<td colspan="4" class="cart_total"><span class="red">TOTAL:</span></td>
				<td>
					<asp:Label runat="server" ID="lTotal" />$</td>
			</tr>

			<tr>
				<td colspan="4" class="cart_total"><span class="red">TOTAL TAX:</span></td>
				<td>
					<asp:Label runat="server" ID="lTotalTax" />$</td>
			</tr>

			<tr>
				<td colspan="4" class="cart_total"><span class="red">TOTAL + TAX:</span></td>
				<td>
					<asp:Label runat="server" ID="lTotalAndTax" />$</td>
			</tr>
		</table>
	</div>


	<div class="title">
		<span class="title_icon">
			<img src="images/bullet1.gif" alt="" title="" /></span>PayPal
	</div>

	<div class="feat_prod_box_details">
		<br />
		<table>
			<tr>
				<td>
					<label>Id:</label>
				</td>
				<td>
					<asp:TextBox runat="server" ID="tbId" />
				</td>
				<td>
					<asp:RequiredFieldValidator runat="server" ControlToValidate="tbId" ErrorMessage="'Id' must be set" ForeColor="Red" />
				</td>
			</tr>
			<tr>
				<td>
					<label>Password:</label>
				</td>
				<td>
					<asp:TextBox runat="server" ID="tbPassword" TextMode="Password" />
				</td>
				<td>
					<asp:RequiredFieldValidator runat="server" ControlToValidate="tbId" ErrorMessage="'Password' must be set" ForeColor="Red" />
				</td>
			</tr>
		</table>

		<div class="title">
			<span class="title_icon">
				<img src="images/bullet1.gif" alt="" title="" /></span>Promo Code
		</div>
	</div>

	<div class="feat_prod_box_details">
		<table>
			<tr>
				<td>
					<label>Code:</label>
				</td>
				<td>
					<asp:TextBox runat="server" ID="tbPromo" />
				</td>
				<td>
					<asp:CustomValidator runat="server" ID="vCode" ErrorMessage="Invalid Code" ForeColor="Red" OnServerValidate="vCode_OnServerValidate" />
				</td>
			</tr>
		</table>
	</div>


	<asp:Button runat="server" CssClass="register" Text="Confirm" ID="bConfirm" OnClick="bConfirm_OnClick" />
	<asp:Button runat="server" CssClass="register" Text="Continue Shopping" ID="bCancel" OnClick="bCancel_OnClick" CausesValidation="False" />
	<asp:Button runat="server" CssClass="register" Text="Back to Cart" ID="bToCart" OnClick="bToCart_OnClick" CausesValidation="False" />

	<div class="clear"></div>
</asp:Content>
