﻿using System;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using BookStore.App_Data;

namespace BookStore
{
	public class MySession
	{
		private const string TagLogedUserId = "I";
		private const string TagLogedUserEmail = "E";
		private const string TagLogedUserPassword = "P";
		private const string TagLogedAdmin = "A";
		private const string CookieName = "Creds";

		private static readonly TimeSpan CookieExpiry = TimeSpan.FromDays(5);

		private static HttpSessionState Session
		{
			get { return HttpContext.Current.Session; }
		}

		private static HttpResponse Response
		{
			get { return HttpContext.Current.Response; }
		}

		private static HttpRequest Request
		{
			get { return HttpContext.Current.Request; }
		}

		public static bool IsUserLoged
		{
			get
			{
				var val = Request.Cookies[CookieName];

				if (val == null || !val.HasKeys)
					return false;

				return true;
			}
		}

		public static bool IsUserAdmin
		{
			get
			{
				var val = Request.Cookies[CookieName];

				if (val == null || !val.HasKeys)
					return false;

				return bool.Parse(val[TagLogedAdmin]);
			}
		}

		public static string LogedUserEmail
		{
			get
			{
				var c = Request.Cookies[CookieName];

				if (c == null || !c.HasKeys)
					return "None";

				return c.Values[TagLogedUserEmail];
			}
		}

		public static int UserOrAdminId { get; private set; }

		public static void RestoreLoginInfo()
		{
			if (!ValidateUser())
				Logout();
		}

		public static bool Login(string email, string password, bool remember)
		{
			bool isAdmin;

			var id = CheckAdmin(email, password);

			if (id == -1)
			{
				isAdmin = false;
				id = CheckUser(email, password);

			}
			else
				isAdmin = true;

			if (id == -1)
				return false;

			UserOrAdminId = id;

			// ReSharper disable once PossibleNullReferenceException
			Response.Cookies[CookieName].Values[TagLogedUserId] = id.ToString();
			Response.Cookies[CookieName].Values[TagLogedUserEmail] = email;
			Response.Cookies[CookieName].Values[TagLogedUserPassword] = password;
			Response.Cookies[CookieName].Values[TagLogedAdmin] = isAdmin.ToString();

			if (remember)
				Response.Cookies[CookieName].Expires = DateTime.Now.Add(CookieExpiry);

			return true;
		}

		public static void Logout()
		{
			RemoveCookie(CookieName);
		}

		public static bool ValidateUser()
		{
			var ck = Request.Cookies[CookieName];

			if (ck == null || !ck.HasKeys)
				return false;

			int id;
			string password;
			string email;
			bool isAdmin;
			try
			{
				id = int.Parse(ck.Values[TagLogedUserId]);
				password = ck.Values[TagLogedUserPassword];
				email = ck.Values[TagLogedUserEmail];
				isAdmin = bool.Parse(ck.Values[TagLogedAdmin]);
			}
			catch (Exception ex)
			{
				//Corrupted cookie
				RemoveCookie(CookieName);
				return false;
			}

			var pId = isAdmin
				          ? CheckAdmin(email, password)
				          : CheckUser(email, password);

			UserOrAdminId = pId;

			var stillValid = pId == id;
			if (stillValid)
				return true;

			Logout();

			return false;
		}

		private static void RemoveCookie(string name)
		{
			// ReSharper disable once PossibleNullReferenceException
			Response.Cookies[name].Expires = DateTime.Now.AddDays(-5);
		}

		private static int CheckAdmin(string email, string password)
		{
			using (var c = new DatabaseContainer())
			{
				var pAdminId = new ObjectParameter("p_adminId", typeof(int));
				c.ADMIN_LOGIN(email, password, pAdminId);
				return (int)(decimal)pAdminId.Value;
			}
		}

		private static int CheckUser(string email, string password)
		{
			using (var c = new DatabaseContainer())
			{
				var pUserId = new ObjectParameter("p_userId", typeof(int));

				c.USERS_LOGIN(email, password, pUserId);
				return (int)(decimal)pUserId.Value;
			}
		}
	}
}