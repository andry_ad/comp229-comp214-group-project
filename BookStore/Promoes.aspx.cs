﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BookStore.App_Data;

namespace BookStore
{
	public partial class Promoes : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack)
				return;

			List<PROMOES_GET_DETAILS_Result> books = null;

			var a = ExceptionHelper.ReturnBusinessErrorOrRedirect(() =>
			{
				using (var c = new DatabaseContainer())
					books = c.PROMOES_GET_DETAILS().ToList();
			},
				"Failed to get promoes info",
				Response);

			switch (a.Status)
			{
				case ExceptionHelper.Status.Success:
					rPromoes.DataSource = books;
					rPromoes.DataBind();
				break;
			}
		}

		protected void bAdd_OnClick(object sender, EventArgs e)
		{
			Response.Redirect("~/EditPromo.aspx");
		}

		protected void rPromoes_OnItemCommand(object source, RepeaterCommandEventArgs e)
		{
			if (e.CommandName != "Edit")
				return;

			Response.Redirect(EditPromo.GetUrlForPromoId(int.Parse((string)e.CommandArgument)));
		}
	}
}