﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="BookStore.About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContent" runat="server">
	<div class="title">
		<span class="title_icon">
			<img src="images/bullet1.gif" alt="" title="" /></span>About us
	</div>

	<div class="feat_prod_box_details">
		<p class="details">
			<img src="images/about.gif" alt="" title="" class="right" />
			Group 2:
			<br />
			Akash Durbha
			<br />
			Andrii Deresh
			<br />
			Asha Vijayakumar Sum
			<br />
			Bhaveshkumar Gangani
			<br />
			Swathi Danesh
			<br />
		</p>
	</div>

	<div class="clear"></div>
</asp:Content>
