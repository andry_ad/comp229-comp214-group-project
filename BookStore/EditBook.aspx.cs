﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BookStore.App_Data;

namespace BookStore
{
	public partial class EditBook : Page
	{
		private const string BookId = "bookId";

		private const string VSIsNewBook = "IsNewBook";
		private const string VSBookId = "BookId";
		private const string VSBookISBN = "BookISBN";

		protected string Isbn { get; private set; }

		protected bool IsNewBook
		{
			get { return (bool)ViewState[VSIsNewBook]; }
			private set { ViewState[VSIsNewBook] = value; }
		}

		protected int? EditBookId
		{
			get { return (int?)ViewState[VSBookId]; }
			private set { ViewState[VSBookId] = value; }
		}

		protected string EditBookISBN
		{
			get { return (string)ViewState[VSBookISBN]; }
			private set { ViewState[VSBookISBN] = value; }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!MySession.IsUserAdmin)
			{
				Response.Redirect("Login.aspx");
				return;
			}

			if(IsPostBack)
				return;

			tbYear.Text = "YYYY";
			vReqYear.InitialValue = tbYear.Text;

			tbSearchCounts.Text = "0";

			vRngYear.MaximumValue = DateTime.Now.Year.ToString();

			using (var c = new DatabaseContainer())
			{
				List<Category> categories;
				try
				{
					categories = c.CATEGORIES_GET().ToList();
				}
				catch
				{
					Response.Redirect(BookStore.Error.GetErrorPageUrl("Failed to get categories"));
					return;
				}

				cblCategories.DataSource = categories;
				cblCategories.DataValueField = "Id";
				cblCategories.DataTextField = "Name";
				cblCategories.DataBind();

				var sId = Request.QueryString[BookId];
				IsNewBook = string.IsNullOrWhiteSpace(sId);
				bSave.Text = IsNewBook
					             ? "Add"
					             : "Update";

				if (!IsNewBook)
				{
					int id;

					if (!int.TryParse(sId, out id))
					{
						Response.Redirect(BookStore.Error.GetErrorPageUrl("Invalid book id"));
						return;
					}

					BOOKS_GET_BY_ID_Result book;

					try
					{
						book = c.BOOKS_GET_BY_ID(id).Single();
					}
					catch
					{
						Response.Redirect(BookStore.Error.GetErrorPageUrl("Invalid book id"));
						return;
					}

					EditBookId = id;
					EditBookISBN = book.ISBN;

					var bookPicture = Path.Combine(Constants.ProductImagesFolder, book.ISBN + Constants.ImageExtension);

					tbTitle.Text = book.Title;
					tbISBN.Text = book.ISBN;
					tbAuthor.Text = book.Author;
					tbPublisher.Text = book.Publisher;
					tbYear.Text = book.Year.Year.ToString();
					tbPrice.Text = book.Price.ToString();
					tbSearchCounts.Text = book.SearchCounts.ToString();

					iBookPicture.ImageUrl = bookPicture;
					iBookPicture.AlternateText = book.ISBN;

					var cat = c.CATEGORIES_GET_FOR_BOOK(id).ToList();

					foreach (var li in cblCategories.Items.OfType<ListItem>())
					{
						var cId = int.Parse(li.Value);

						if (cat.Any(result => result.Id == cId))
							li.Selected = true;
					}

					//update values
				}
			}
		}

		public static string GetUrlForBookId(int id)
		{
			return string.Format("EditBook.aspx?{0}={1}", BookId, id);
		}

		protected void bSave_OnClick(object sender, EventArgs e)
		{
			if(!Page.IsValid)
				return;

			try
			{
				if (IsNewBook)
				{
					var tempPictureName = iBookPicture.ImageUrl;

					if (string.IsNullOrWhiteSpace(tempPictureName)
					    || !File.Exists(Path.Combine(HttpRuntime.AppDomainAppPath, tempPictureName)))
					{
						Response.Redirect(BookStore.Error.GetErrorPageUrl("Internal error. Book image not found!"));
						return;
					}
					int id = -1;

					using (var c = new DatabaseContainer())
					{
						var pbookId = new ObjectParameter("p_bookId", typeof(int));

						c.BOOKS_ADD(tbTitle.Text,
							tbISBN.Text,
							tbAuthor.Text,
							tbPublisher.Text,
							new DateTime(int.Parse(tbYear.Text), 1, 1),
							decimal.Parse(tbPrice.Text),
							int.Parse(tbSearchCounts.Text),
							pbookId);

						id = (int)(decimal)pbookId.Value;

						foreach (var li in cblCategories.Items.OfType<ListItem>().Where(item => item.Selected))
							c.CATEGORIES_BOOK_ADD(id, int.Parse(li.Value));
					}

					var newPictureName = Path.Combine(Constants.ProductImagesFolder, tbISBN.Text + Constants.ImageExtension);
					File.Move(Path.Combine(HttpRuntime.AppDomainAppPath, tempPictureName),
						Path.Combine(HttpRuntime.AppDomainAppPath, newPictureName));
				}
				else
				{
					var tempPictureName = iBookPicture.ImageUrl;

					if (string.IsNullOrWhiteSpace(tempPictureName)
						|| !File.Exists(Path.Combine(HttpRuntime.AppDomainAppPath, tempPictureName)))
					{
						Response.Redirect(BookStore.Error.GetErrorPageUrl("Internal error. Book image not found!"));
						return;
					}

					if (!EditBookId.HasValue || string.IsNullOrWhiteSpace(EditBookISBN))
					{
						Response.Redirect(BookStore.Error.GetErrorPageUrl("Internal error. Cannot retreive edit book info"));
						return;
					}

					using (var c = new DatabaseContainer())
					{
						c.BOOKS_UPDATE(EditBookId, tbTitle.Text,
							tbISBN.Text,
							tbAuthor.Text,
							tbPublisher.Text,
							new DateTime(int.Parse(tbYear.Text), 1, 1),
							decimal.Parse(tbPrice.Text),
							int.Parse(tbSearchCounts.Text));

						c.CATEGORIES_BOOK_CLEAR(EditBookId);
						foreach (var li in cblCategories.Items.OfType<ListItem>().Where(item => item.Selected))
							c.CATEGORIES_BOOK_ADD(EditBookId, int.Parse(li.Value));
					}
					var oldPictureName = Path.Combine(Constants.ProductImagesFolder, EditBookISBN + Constants.ImageExtension);


					var newPictureName = Path.Combine(Constants.ProductImagesFolder, tbISBN.Text + Constants.ImageExtension);
					var newPictureFn = Path.Combine(HttpRuntime.AppDomainAppPath, newPictureName);

					var tempPictureFn = Path.Combine(HttpRuntime.AppDomainAppPath, tempPictureName);
					//picture was changed
					if (!tempPictureName.Equals(oldPictureName))
					{
						if (File.Exists(newPictureFn))
							File.Delete(newPictureFn);

						File.Move(tempPictureFn, newPictureFn);
					}
					else
					{
						//isbn was changed - need to change original image name
						if (!EditBookISBN.Equals(tbISBN.Text))
							File.Move(tempPictureFn, newPictureFn);
					}
				}
			}
			catch
			{
				Response.Redirect(BookStore.Error.GetErrorPageUrl("Failed to save book"));
			}

			Response.Redirect("Inventory.aspx");
		}

		protected void bUpload_OnClick(object sender, EventArgs e)
		{
			if (fuImage.HasFile)
			{
				var fileName = Server.HtmlEncode(fuImage.FileName);
				var extension = Path.GetExtension(fileName);
				if (extension != Constants.ImageExtension)
				{
					lFileSatuts.Text = "File extension should be " + Constants.ImageExtension;
					return;
				}

				var newFileName = Path.Combine(Constants.ProductImagesTempFolder, Guid.NewGuid() + Constants.ImageExtension);

				try
				{
					fuImage.SaveAs(Path.Combine(HttpRuntime.AppDomainAppPath, newFileName));
					lFileSatuts.Text = "Image downloaded";

					iBookPicture.ImageUrl = newFileName;
				}
				catch
				{
					lFileSatuts.Text = "Fialed to upload file";
				}
			}
			else
				lFileSatuts.Text = "No file is selected";
		}

		protected void vImageUploaded_OnServerValidate(object source, ServerValidateEventArgs args)
		{
			args.IsValid = !string.IsNullOrWhiteSpace(iBookPicture.ImageUrl);
		}

		protected void vUniqueISBN_OnServerValidate(object source, ServerValidateEventArgs args)
		{
			var id = -1;

			if (!IsNewBook)
				id = EditBookId.Value;

			using (var c = new DatabaseContainer())
			{
				var pisUnique = new ObjectParameter("p_isUnique", typeof(int));
				c.BOOKS_IS_ISBN_UNIQUE(tbISBN.Text, id, pisUnique);

				args.IsValid = ((decimal)pisUnique.Value != 0);
			}
		}
	}
}