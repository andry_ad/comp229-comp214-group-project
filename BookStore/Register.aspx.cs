﻿using System;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using BookStore.App_Data;

namespace BookStore
{
	public partial class Register : Page
	{
		private readonly string[] _provinces =
		{
			"Alberta", "British Columbia", "Manitoba", "New Brunswick",
			"Newfoundland and Labr.", "Nova Scotia", "Ontario", "Prince Edward Island", "Quebec", "Saskatchewan"
		};

		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack)
				return;

			ddState.DataSource = _provinces;
			ddState.DataBind();

			vRBirthday.MinimumValue = new DateTime(1990, 1, 1).ToShortDateString();
			vRBirthday.MaximumValue = DateTime.Now.AddYears(-5).ToShortDateString();

			vBirthday.InitialValue = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;
			tbBirthday.Text = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;
		}

		protected void vUniqueEmail_OnServerValidate(object source, ServerValidateEventArgs args)
		{
			if (string.IsNullOrWhiteSpace(args.Value))
				args.IsValid = true; //empty value cannot be checked
			else
			{
				var isUnque = new ObjectParameter("p_isUnique", typeof(int));
				using (var c = new DatabaseContainer())
					c.CHECK_UNQIUE_EMAIL(args.Value, isUnque);

				args.IsValid = (int)(decimal)isUnque.Value > 0;
			}
		}

		protected void OnClick(object sender, EventArgs e)
		{
			if(!Page.IsValid)
				return;

			//todo ad: add validation handling

			try
			{
				using (var c = new DatabaseContainer())
					c.USERS_REGISTER(tbEmail.Text,
						tbPassword.Text,
						tbFirstName.Text,
						tbLastName.Text,
						DateTime.Parse(tbBirthday.Text),
						DateTime.Now,
						tbPhone.Text,
						tbAddress.Text,
						tbPostalCode.Text,
						tbCity.Text,
						ddState.Text);
			}
			catch (Exception ex)
			{

				return;
			}

			Response.Redirect("Login.aspx");
		}
	}
}