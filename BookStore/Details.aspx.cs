﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BookStore.App_Data;

namespace BookStore
{
	public partial class Details : System.Web.UI.Page
	{
		private const string BookId = "bookId";

		protected BOOKS_GET_BY_ID_Result BookInfo { get; private set; }
		protected ObjectResult<CATEGORIES_GET_FOR_BOOK_Result> BookCategories { get; private set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			var id = -1;

			try
			{
				var sId = Request.QueryString[BookId];

				id = int.Parse(sId);
			}
			catch 
			{
				Response.Redirect(BookStore.Error.GetErrorPageUrl("Product is not found!"));
				return;
			}

			try
			{
				using (var c = new DatabaseContainer())
				{
					BookInfo = c.BOOKS_GET_BY_ID(id).Single();
					BookCategories = c.CATEGORIES_GET_FOR_BOOK(id);
				}
			}
			catch 
			{
				Response.Redirect(BookStore.Error.GetErrorPageUrl("Error while retrieving product information"));
				return;
			}
		}

		public static string GetUrlForBookId(int id)
		{
			return string.Format("Details.aspx?{0}={1}", BookId, id);
		}

		protected void bAddToCart_OnClick(object sender, EventArgs e)
		{
			if (!MySession.IsUserLoged)
			{
				Response.Redirect("Login.aspx");
				return;
			}

			using (var c = new DatabaseContainer())
			{
				c.BASKET_ADD(MySession.UserOrAdminId, BookInfo.Id, int.Parse(tbQuantity.Text));
			}

			Response.Redirect("Index.aspx");
		}

		protected void bContinue_OnClick(object sender, EventArgs e)
		{
			Response.Redirect("Index.aspx");
		}
	}
}