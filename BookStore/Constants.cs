﻿using System;
using System.Data.Entity.Core;
using Oracle.ManagedDataAccess.Client;

namespace BookStore
{
	public class Constants
	{
		public const string ImageExtension = ".jpg";
		public const string ProductImagesFolder = "productImages";
		public const string ProductImagesTempFolder = "productImages\\temp";

		public static string ErrorFieldMustBeSet(string fieldName)
		{
			return string.Format("'{0}' field must be set", fieldName);
		}

		public static bool IsBusinessLogicError(int number)
		{
			return 20000 <= number && number <= 20999;
		}
	}
}