﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using BookStore.App_Data;

namespace BookStore
{
	public class Global : System.Web.HttpApplication
	{
		protected void Application_Start(object sender, EventArgs e)
		{
			//check if connection is valid
			using (var c = new DatabaseContainer())
				c.Database.Connection.Open();


			//remove temp images
			//todo ad
			/*
			if (!Directory.Exists(Constants.ProductImagesTempFolder))
				Directory.CreateDirectory(Constants.ProductImagesTempFolder);

			try
			{
				foreach (var f in Directory.GetFiles(Constants.ProductImagesTempFolder))
				{
					File.Delete(f);
				}
			}
			catch 
			{
			}
			*/
		}

		protected void Session_Start(object sender, EventArgs e)
		{
			//check if session is from cookies
			MySession.RestoreLoginInfo();
		}

		protected void Application_BeginRequest(object sender, EventArgs e)
		{

		}

		protected void Application_AuthenticateRequest(object sender, EventArgs e)
		{

		}

		protected void Application_Error(object sender, EventArgs e)
		{

		}

		protected void Session_End(object sender, EventArgs e)
		{

		}

		protected void Application_End(object sender, EventArgs e)
		{

		}
	}
}