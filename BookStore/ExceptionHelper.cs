﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using System.Web;
using System.Web.Management;
using Oracle.ManagedDataAccess.Client;

namespace BookStore
{
	public class ExceptionHelper
	{
		public enum Status
		{
			Success,
			BusinessError,
			InternalError
		}

		public class Result
		{
			public Status Status { get; private set; }
			public string Error { get; private set; }

			public Result(Status status, string businessError)
			{
				Status = status;
				Error = businessError;
			}
			
			public static Result Success()
			{
				return new Result(Status.Success, null);
			}

			public static Result BusinessError(string message)
			{
				return new Result(Status.BusinessError, message);
			}

			public static Result InternalError(string message)
			{
				return new Result(Status.InternalError, message);
			}
		}

		public static Result ReturnBusinessErrorOrRedirect(Action action, string errorMessage, HttpResponse response)
		{
			try
			{
				action.Invoke();

				return Result.Success();
			}
			catch (Exception ex)
			{
				var oEx = ex.InnerException as OracleException;
				if (oEx == null)
				{
					response.Redirect(BookStore.Error.GetErrorPageUrl(errorMessage));
					return Result.InternalError(ex.Message);
				}
				else
				{
					if (Constants.IsBusinessLogicError(oEx.Number))
						return Result.BusinessError(oEx.Message); //todo ad: decrypt message

					response.Redirect(BookStore.Error.GetErrorPageUrl(errorMessage));
					return Result.InternalError(ex.Message);	
				}
			}
		}
	}
}