﻿namespace BookStore
{
	public class CartDetails
	{
		public int TotalQty { get; set; }
		public decimal TotalPrice { get; set; }
		public decimal Discount { get; set; }
		public decimal TotalTax { get; set; }
		public decimal TotalGrand { get; set; }

		public CartDetails(int totalQty, decimal totalPrice, decimal totalTax, decimal totalGrand, decimal discount)
		{
			TotalQty = totalQty;
			TotalPrice = totalPrice;
			TotalTax = totalTax;
			TotalGrand = totalGrand;
			Discount = discount;
		}
	}
}